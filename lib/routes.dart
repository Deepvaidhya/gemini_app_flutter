import 'package:flutter/material.dart';
import 'package:gemini_app/pages/home/home.dart';
import 'package:gemini_app/pages/login/login_screen.dart';
import 'package:gemini_app/pages/onward/welcome.dart';
import 'package:gemini_app/pages/reminder-setup.dart';


final routes = {
  '/login':         (BuildContext context) => new Login(),
  '/welcome':       (BuildContext context) => new Welcome(),
  '/home':          (BuildContext context) => new Home(),
  '/reminder':      (BuildContext context) => new Reminder(),
  '/' :             (BuildContext context) => new Welcome(),
  
};