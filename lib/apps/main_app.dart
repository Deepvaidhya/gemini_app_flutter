import 'package:flutter/material.dart';
import 'package:gemini_app/widgets/web_upload.dart';

class UploadApp extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
     
    return MaterialApp(
    
       title: 'My App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'Poopins',
        primarySwatch: Colors.blue,
      ),
      home:  FileUploadApp(),
      
       
 
    );
  } 
 


}