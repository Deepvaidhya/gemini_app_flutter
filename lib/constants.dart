enum AlertAction {
  cancel,
  discard,
  disagree,
  agree,
}

const String apiURL = "https://pub1.brightoutcome-dev.com/wakeful/admin/Api/auth";
const bool devMode = false;
const double textScaleFactor = 1.0;