//
// Generated file. Do not edit.
//

import 'package:flutter_web_image_picker/flutter_web_image_picker.dart';
import 'package:fluttertoast/fluttertoast_web.dart';
import 'package:image_picker_web/image_picker_web.dart';
import 'package:shared_preferences_web/shared_preferences_web.dart';

import 'package:flutter_web_plugins/flutter_web_plugins.dart';

// ignore: public_member_api_docs
void registerPlugins(Registrar registrar) {
  FlutterWebImagePicker.registerWith(registrar);
  FluttertoastWebPlugin.registerWith(registrar);
  ImagePickerWeb.registerWith(registrar);
  SharedPreferencesPlugin.registerWith(registrar);
  registrar.registerMessageHandler();
}
