import 'dart:async';
import 'dart:convert';

import 'package:gemini_app/utils/network_util.dart';
//import 'package:gemini_app/models/user.dart';
import 'package:gemini_app/models/login_model.dart';

class RestDatasource {
  NetworkUtil _netUtil = new NetworkUtil();
  // ignore: non_constant_identifier_names
  static final BASE_URL = "https://pub1.brightoutcome-dev.com/wakeful/admin/Api/auth";
  // ignore: non_constant_identifier_names
  static final LOGIN_URL = BASE_URL + "/login";
  // ignore: non_constant_identifier_names
  static final _API_KEY = "YXBwVXNlcjpwd2FBcHBzQDIwMTg=";
   //bool remember_me = false;
  Future<LoginModel> login(String username, String password) {
    String jwt;
    return _netUtil.post(LOGIN_URL, body:json.encode({
      "username": username,
      "password": password,
      "remember_me": username,
      "course" : "1"
    }), 
    headers: <String, String>{'content-type': 'application/x-www-form-urlencoded', 'Authorization': 'Basic ' + _API_KEY}).then((dynamic res) {
      LoginModel response = new LoginModel.fromJson(res);
      

      if(res['status'] == 'success'){
        return new LoginModel.fromJson(res);        
      }
      else {
        print("error" + res['msg']);
        throw new Exception(response.error.message);
      }
  });
  }
}
