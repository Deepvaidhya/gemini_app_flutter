import 'package:flutter/material.dart';

class Progress extends StatefulWidget {
  @override
  _ProgressState createState() => _ProgressState();
}

class _ProgressState extends State<Progress> {
  BuildContext _ctx;
  Image pickedImage;
  Image image;
  final txtcolor = const Color.fromRGBO(38, 62, 114, 1);

  @override
  Widget build(BuildContext context) {
    _ctx = context;

    final next = Material(
     elevation: 8.0,     
      borderRadius: BorderRadius.circular(30.0),
      color: txtcolor,
      child: MaterialButton(
      padding: EdgeInsets.fromLTRB(65.0, 18.0, 65.0, 18.0),
      height: 40.0,
      minWidth: 60.0,
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => Progress(),
              fullscreenDialog: true),
        );
      },
      textColor: Color.fromRGBO(251, 251, 251, 1),
      child: Text("complete pre-visit check-in", style: TextStyle(fontWeight: FontWeight.bold),textAlign: TextAlign.center),
      ),
    );
    
    final description = Padding(
      padding: EdgeInsets.all(8.0),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: 'You are currently on Class 6 but you still have some activities to finish for previous weeks. See below.',
          style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1).withOpacity(0.7),fontFamily:'Poppins',fontSize: 14)
        ),
      ),
    );

    return new Scaffold(
      body: new Center(
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[                 
            new Text("Welcome back, Melissa!",
              style: TextStyle(fontWeight: FontWeight.bold,color: Color.fromRGBO(38, 62, 114, 1),fontFamily:'Poppins',fontSize: 20),textAlign: TextAlign.center,
            ), 
            Container(
              margin: const EdgeInsets.only(top: 2.0),
              padding: EdgeInsets.all(10.0),
              child: description,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: next,
            )
          ],
        ),
      ),
    );

  }
}