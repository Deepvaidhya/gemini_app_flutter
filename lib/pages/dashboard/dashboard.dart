import 'package:flutter/material.dart';
import './FirstScreen.dart'; 
import './Progress.dart'; 


class Dashboard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _DashboardState();
  }
}

class _DashboardState extends State<Dashboard> with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {    
    
    return Scaffold(
      appBar: AppBar(
        title: Text("Hi, Melissa", style: TextStyle(fontSize: 18, color: Colors.white,fontFamily:'Poppins')), 
        elevation: 18.0, 
        centerTitle: true,
        backgroundColor:Color.fromRGBO(38, 62, 114, 1),
        actions: [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(Icons.email,color: Colors.white,),            
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(Icons.notifications,color: Colors.white,),
          ),
        ],
      ),
      drawer: Drawer(),
      body: Container(        
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [     
            Padding(
              padding: const EdgeInsets.only(top:18.0),
              child: TabBar(
                unselectedLabelColor: Color.fromRGBO(38, 62, 114, 1),
                labelColor: Colors.white,
                indicatorColor: Color.fromRGBO(38, 62, 114, 1),
                indicator: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: Color.fromRGBO(38, 62, 114, 1)),
                tabs: [
                  Tab(text: 'DASHBOARD'),
                  Tab(text: 'PROGRESS'),
                  Tab(text: 'COMMUNITY')
                ],
                controller: _tabController,
                indicatorSize: TabBarIndicatorSize.tab,
              ),
            ),
            Expanded(
              child: TabBarView(
                children: [
                  FirstScreen(),  
                  Progress(), 
                  Text('COMMUNITY')
                ],
                controller: _tabController,
              ),
            ),
          ],
        ),
      ),
    );
  }
}