import 'package:flutter/material.dart';
import 'package:gemini_app/pages/profile/profile.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _HomeState();
  }
}

class _HomeState extends State<Home> {
  BuildContext _ctx;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    _ctx = context;

    final avatar = Container(
          margin: EdgeInsets.all(20),
          width: 120,
          height: 120,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              image: NetworkImage('https://googleflutter.com/sample_image.jpg'),
              fit: BoxFit.fill
            ),
          ),
    );

    
    final description = Padding(
      padding: EdgeInsets.all(10),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam gravida magna nec enim aliquet, vel congue enim aliquam. Donec id congue sapien. Praesent molestie volutpat nisi, ac convallis ligula egestas ut. Proin eget rutrum turpis, eu sodales lectus. Vestibulum volutpat felis ligula, sed sagittis nibh feugiat at.',
          style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1).withOpacity(0.7), fontFamily:'Poppins',fontSize: 14,height: 1.5)
        ),
      ),
    );

    final start = Material(
      elevation: 8.0,      
      borderRadius: BorderRadius.circular(30.0),
      color: Theme.of(context).accentColor,
      child: MaterialButton(
      padding: EdgeInsets.fromLTRB(95.0, 18.0, 95.0, 18.0),
      height: 40.0,
      minWidth: 60.0,
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => Profile(),
              fullscreenDialog: true),
        );
      },
      textColor: Color.fromRGBO(38, 62, 114, 1),
      child: Text("START", style: TextStyle(fontWeight: FontWeight.bold,fontFamily:'Poppins'),textAlign: TextAlign.center),
      ),
    );

    return new Scaffold(
      body: new Center(
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [            
          avatar,
          new Text(
            "Welcome, Melissa",
            style: TextStyle(fontWeight: FontWeight.bold,fontFamily:'Poppins',color: Color.fromRGBO(38, 62, 114, 1),fontSize: 26),
          ), 
          Container(
            margin: const EdgeInsets.only(top: 5.0),
            child: new Padding(
              padding: const EdgeInsets.all(30.0),
              child: description,
            ),
          ),
          start,
          ],
        ),
      ),
    );
  }

  
}
