import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/gestures.dart';
import 'package:gemini_app/pages/profile/edit_profile.dart';

class Contact extends StatefulWidget {
  ContactState createState() => ContactState();
}

class ContactState extends State<Contact> {
  BuildContext _ctx; 

  bool isLoggedIn = false;
  String name = '';

  @override
  void initState() {
    super.initState();
  }

  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  String _password, _username, _fname, _lname,_message;

  void _submit() {
    final form = formKey.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
    }
  }
 
  final color = const Color(0xffECEFF1);

  @override
  Widget build(BuildContext context) {
    
    _ctx = context;

    final send = Material(
      elevation: 8.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Theme.of(context).accentColor,
      child: MaterialButton(
      height: 40.0,
      minWidth: 60.0,
      //minWidth: MediaQuery.of(context).size.width,
      padding: EdgeInsets.fromLTRB(95.0, 18.0, 95.0, 18.0),
      onPressed: _submit,
      textColor: Color.fromRGBO(38, 62, 114, 1),
      child: Text("send".toUpperCase(), style: TextStyle(fontWeight: FontWeight.bold),textAlign: TextAlign.center),
      ),
    );

    final next = Material(
     elevation: 8.0,     
      borderRadius: BorderRadius.circular(30.0),
      color: Color.fromRGBO(38, 62, 114, 1),
      child: MaterialButton(
      padding: EdgeInsets.fromLTRB(65.0, 18.0, 65.0, 18.0),
      height: 40.0,
      minWidth: 60.0,
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => EditProfile(),
              fullscreenDialog: true),
        );
      },
      textColor: Color.fromRGBO(251, 251, 251, 1),
      child: Text("next".toUpperCase(), style: TextStyle(fontWeight: FontWeight.bold),textAlign: TextAlign.center),
      ),
    );

    var form = new Column(
      children: <Widget>[
        new Form(
          key: formKey,
          child: Container(
            margin: const EdgeInsets.only(bottom: 10.0),
            padding: const EdgeInsets.all(8.0),
            child: new Column(
              children: <Widget>[
                 new TextFormField(
                    onSaved: (val) => _fname = val,
                    validator: (val) {
                      if (val.isEmpty) {
                        return 'Please enter your name';
                      }
                      return null;
                    },
                    decoration: InputDecoration(                      
                      labelText: 'Your name',
                      labelStyle: TextStyle(
                          color: Color.fromRGBO(153, 153, 153, 1),
                          fontSize: 16
                      ),
                    ),
                    ),                 
                new TextFormField(
                    onSaved: (val) => _username = val,
                    validator: (val) {
                      return !EmailValidator.validate(val, true)
                          ? "Please enter a valid email"
                          : null;
                    },
                    keyboardType: TextInputType.emailAddress,
                    style: new TextStyle(
                      fontFamily: "Poppins",
                    ),
                    decoration: new InputDecoration(
                       labelStyle: TextStyle(
                          color: Color.fromRGBO(153, 153, 153, 1),
                          fontSize: 16
                      ),
                      labelText: "Email address",                     
                    ),
                  ),
                new TextFormField(
                    obscureText: true,
                    onSaved: (val) => _password = val,
                    validator: (val) {
                      return val.length < 1
                          ? "Please enter valid mobile number"
                          : null;
                    },
                    style: new TextStyle(
                      fontFamily: "Poppins",
                      color: Color.fromRGBO(153, 153, 153, 1),
                    ),
                    decoration: new InputDecoration(
                       labelStyle: TextStyle(
                          color: Color.fromRGBO(153, 153, 153, 1),
                          fontSize: 16
                      ),
                      labelText: 'Your phone number',
                    ),
                ),
                new TextFormField(
                  minLines: 6, // any number you need (It works as the rows for the textarea)
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  textAlign: TextAlign.center,
                  //  obscureText: true,
                  //   onSaved: (val) => _message = val,
                  //   validator: (val) {
                  //     return val.length < 20
                  //         ? "Please enter message"
                  //         : null;
                  //   },
                    style: new TextStyle(
                      fontFamily: "Poppins",
                      color: Color.fromRGBO(153, 153, 153, 1),
                    ),
                  decoration: new InputDecoration(
                      labelStyle: TextStyle(
                        color: Color.fromRGBO(153, 153, 153, 1),
                        fontSize: 16,                      
                    ),
                    labelText: 'Use this space to write your message',
                  ),
                )
                 
              ],
            ),
          ),
        ),

        _isLoading ? new CircularProgressIndicator(  backgroundColor: color, valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),) : send
      ],
      crossAxisAlignment: CrossAxisAlignment.center,
    );
    
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: new Image.asset('left-arrow-w.png'),
            tooltip: 'Back',
            onPressed: () => Navigator.of(context).pop(),
        ), 
        title: Text("Contact Us".toUpperCase(), style: TextStyle(fontSize: 18, color: Colors.white,fontFamily:'Poppins')), 
        elevation: 18.0, 
        centerTitle: true,
        backgroundColor:Color.fromRGBO(38, 62, 114, 1),
        actions: [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(Icons.email,color: Colors.white,),            
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(Icons.notifications,color: Colors.white,),
          ),
        ],
      ),
        key: scaffoldKey,
        backgroundColor: Colors.white,
        body: SafeArea(
          child: ListView(
            padding: const EdgeInsets.all(36.0),
            children: <Widget> [
              SizedBox(height: 60.0,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  form, 
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: next,
                  )                 
                ],
              ),
            ],
          ),
        ));
  }
}


