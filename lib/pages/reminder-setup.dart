import 'package:flutter_switch/flutter_switch.dart';
import 'package:flutter/material.dart';
import 'package:gemini_app/pages/home/home.dart';


class Reminder extends StatefulWidget {
  @override
  _ReminderState createState() => _ReminderState();
}

class _ReminderState extends State<Reminder> {

  bool status1 = false;
  bool status2 = false;
  bool status3 = false;
  bool status4 = false;
  bool status5 = false;

  @override
  Widget build(BuildContext context) {

    final _screenSize = MediaQuery.of(context).size;
    
    final avatar = Container(
          margin: EdgeInsets.all(5.0),
          width: 80,
          height: 100,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              image: NetworkImage('https://googleflutter.com/sample_image.jpg'),
              fit: BoxFit.fill
            ),
          ),
    );

    
    final description = Padding(
      padding: EdgeInsets.all(10),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: 'You will be able to change these preferences later in your account settings.',
          style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1).withOpacity(0.7), fontFamily:'Poppins',fontSize: 14,height: 1.5)
        ),
      ),
    );

    final start = Material(
      elevation: 8.0,      
      borderRadius: BorderRadius.circular(30.0),
      color: Theme.of(context).accentColor,
      child: MaterialButton(
      padding: EdgeInsets.fromLTRB(95.0, 18.0, 95.0, 18.0),
      height: 40.0,
      minWidth: 60.0,
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => Home(),
              fullscreenDialog: true),
        );
      },
      textColor: Color.fromRGBO(38, 62, 114, 1),
      child: Text("NEXT", style: TextStyle(fontWeight: FontWeight.bold,fontFamily:'Poppins'),textAlign: TextAlign.center),
      ),
    );

    return Scaffold(
      appBar: new AppBar(
        leading: IconButton(
          icon: new Image.asset('left-arrow.png'),
          tooltip: 'Back',
          onPressed: () => Navigator.of(context).pop(),
        ), 
      ),

      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          avatar,
          new Text(
            "Turn on notifications to get the most out of GEMINI",
            style: TextStyle(fontWeight: FontWeight.bold,fontFamily:'Poppins',color: Color.fromRGBO(38, 62, 114, 1),fontSize: 20),textAlign: TextAlign.center,
          ), 
          Container(
            margin: const EdgeInsets.only(top: 5.0),
            child: new Padding(
              padding: const EdgeInsets.all(10.0),
              child: description,
            ),
          ),          
          SizedBox(height:2.0),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[         
                Container(
                  alignment: Alignment.centerRight,
                  child: Text(
                    "Health Check-in Reminders",
                    style: TextStyle(fontWeight: FontWeight.bold,fontFamily:'Poppins',color: Color.fromRGBO(51, 51, 51, 1),fontSize: 16),textAlign: TextAlign.right,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: FlutterSwitch(
                    activeColor: Color.fromRGBO(38, 62, 114, 1),
                    value: status1,
                    onToggle: (val) {
                      setState(() {
                        status1 = val;
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height:2.0),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[         
                Container(
                  alignment: Alignment.centerRight,
                  child: Text(
                    "Learn Reminders",
                    style: TextStyle(fontWeight: FontWeight.bold,fontFamily:'Poppins',color: Color.fromRGBO(51, 51, 51, 1),fontSize: 16),textAlign: TextAlign.right,
                  ),
                ),   
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FlutterSwitch(
                    activeColor: Color.fromRGBO(38, 62, 114, 1),
                    value: status2,
                    onToggle: (val) {
                      setState(() {
                        status2 = val;
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height:2.0),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[         
                Container(
                  alignment: Alignment.centerRight,
                  child: Text(
                    "Body Practice Reminders",
                    style: TextStyle(fontWeight: FontWeight.bold,fontFamily:'Poppins',color: Color.fromRGBO(51, 51, 51, 1),fontSize: 16),textAlign: TextAlign.right,
                  ),
                ),             
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FlutterSwitch(
                    activeColor: Color.fromRGBO(38, 62, 114, 1),
                    value: status3,
                    onToggle: (val) {
                      setState(() {
                        status3 = val;
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height:2.0),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[         
                Container(
                  alignment: Alignment.centerRight,
                  child: Text(
                    "Mind Practice Reminders",
                    style: TextStyle(fontWeight: FontWeight.bold,fontFamily:'Poppins',color: Color.fromRGBO(51, 51, 51, 1),fontSize: 16),textAlign: TextAlign.right,
                  ),
                ),             
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FlutterSwitch(
                    activeColor: Color.fromRGBO(38, 62, 114, 1),
                    value: status4,
                    onToggle: (val) {
                      setState(() {
                        status4 = val;
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height:2.0),
          Container(
           child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[         
              Container(
                alignment: Alignment.centerRight,
                child: Text(
                  "Community Reminders",
                  style: TextStyle(fontWeight: FontWeight.bold,fontFamily:'Poppins',color: Color.fromRGBO(51, 51, 51, 1),fontSize: 16),textAlign: TextAlign.right,
                ),
              ),             
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: FlutterSwitch(
                  activeColor: Color.fromRGBO(38, 62, 114, 1),
                  value: status5,
                  onToggle: (val) {
                    setState(() {
                      status5 = val;
                    });
                  },
                ),
              ),
            ],
          ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: start,
          ),
          
        ],
      ),
    );
  }

  
}