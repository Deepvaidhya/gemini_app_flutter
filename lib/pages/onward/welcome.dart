import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:gemini_app/pages/login/login_screen.dart';
import 'package:gemini_app/pages/signup/signup.dart';
class Welcome extends StatefulWidget
{
     @override
     _WelcomeState createState() => _WelcomeState();

}
final color = const  Color.fromRGBO(38, 62, 114, 1);
bool _isLoading = false;
class _WelcomeState extends State<Welcome>
{    
    @override
    Widget build(BuildContext context)
    {
      final avatar = Container(
          margin: EdgeInsets.all(20),
          width: 120,
          height: 120,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              image: NetworkImage('https://googleflutter.com/sample_image.jpg'),
              fit: BoxFit.fill
            ),
          ),
      );

      final login = Container(            
            margin: const EdgeInsets.only(top: 20.0),
            padding: EdgeInsets.all(10),
            child: Center(
              child: RichText(
                text: TextSpan(
                    text: 'Already have an account?',
                    style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 1).withOpacity(0.6), fontSize: 14,fontFamily:'Poppins',fontWeight: FontWeight.bold),
                    children: <TextSpan>[
                      TextSpan(text: ' Sign in',
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1).withOpacity(0.9), fontSize: 14,fontWeight: FontWeight.bold),
                              recognizer: TapGestureRecognizer()
                              ..onTap = () async {
                                _isLoading = await Navigator.push(context,
                                    MaterialPageRoute(builder: (context) => Login()));
                              },
                      )
                    ]
                ),
              ),
            )
            );

      final signup = Material(
        elevation: 8.0,
        borderRadius: BorderRadius.circular(30.0),
        color: Theme.of(context).accentColor,
          child: MaterialButton(
          height: 40.0,
          minWidth: 60.0,
          padding: EdgeInsets.fromLTRB(95.0, 18.0, 95.0, 18.0),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => Signup(),
                fullscreenDialog: true),
            );
          },
          textColor: color,
          child: Text("SIGN UP",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14),textAlign: TextAlign.center),
          ),
      );

      final description = Padding(
        padding: EdgeInsets.all(10),
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam gravida magna nec enim aliquet, vel congue enim aliquam. Donec id congue sapien. Praesent molestie volutpat nisi, ac convallis ligula egestas ut.',
            style: TextStyle(color: Color.fromRGBO(255, 255, 255, 1).withOpacity(0.6), fontSize: 14,fontFamily:'Poppins',height: 1.5)
          ),
        ),
      );

      return Scaffold(                  
            backgroundColor:color,
            body: Container(
              padding: EdgeInsets.all(40.0),
              child:ListView(children: <Widget>[
                avatar,
                Container(
                  height: 72,
                  width: 200,
                  child:Center(child:Text(
                  'Welcome to Gemini',
                  style: TextStyle(fontWeight: FontWeight.bold,fontSize: 26,fontFamily:'Poppins',color:Colors.white,),
                  
                ))),
              description,
                Container(
                  margin: const EdgeInsets.only(top: 20.0,bottom: 20.0),
                  child:Wrap(
                      spacing: 10,
                      alignment: WrapAlignment.center,                 
                      children: <Widget>[
                        GestureDetector(
                        onTap: () {Navigator.push(context,MaterialPageRoute(
                            builder: (context)=>Welcome()
                            ));},
                        child: ClipOval(
                          child: Container(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            height: 14.0,
                            width: 14.0,
                          ),
                        ),
                      ),
                       GestureDetector(
                        onTap: () {Navigator.push(context,MaterialPageRoute(
                            builder: (context)=>Secondpage()
                            ));},
                        child: ClipOval(
                          child: Container(
                            color: Colors.black12,
                            height: 14.0,
                            width: 14.0,
                          ),
                        ),
                      ),
                       GestureDetector(
                        onTap: () {Navigator.push(context,MaterialPageRoute(
                            builder: (context)=>Thirdpage()
                            ));},
                        child: ClipOval(
                          child: Container(
                            color:Colors.black12,
                             height: 14.0,
                            width: 14.0,
                          ),
                        ),
                      ),
                       GestureDetector(
                        onTap: () {Navigator.push(context,MaterialPageRoute(
                            builder: (context)=>Fourthpage()
                            ));},
                        child: ClipOval(
                          child: Container(
                            color:Colors.black12,
                            height: 14.0,
                            width: 14.0,
                          ),
                        ),
                      ),

                      ]
                  ),
                ),
              signup,
              login
                
       ])),  
      );
    }

}

class Secondpage extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
     final avatar = Container(
          margin: EdgeInsets.all(20),
          width: 120,
          height: 120,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              image: NetworkImage('https://googleflutter.com/sample_image.jpg'),
              fit: BoxFit.fill
            ),
          ),
    );

   final signup = Material(
          borderRadius: BorderRadius.circular(30.0),
          color: Color.fromRGBO(255, 154, 66, 1),
          child: MaterialButton(
          height: 40.0,
          minWidth: 60.0,
          padding: EdgeInsets.fromLTRB(95.0, 18.0, 95.0, 18.0),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => Signup(),
                fullscreenDialog: true),
            );
          },
          textColor: color,
          child: Text("SIGN UP",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14),textAlign: TextAlign.center),
          ),
      );

    final login = Container(            
      margin: const EdgeInsets.only(top: 20.0),
      padding: EdgeInsets.all(10),
      child: Center(
        child: RichText(
          text: TextSpan(
              text: 'Already have an account?',
              style: TextStyle(
                  color: Color.fromRGBO(255, 255, 255, 1).withOpacity(0.6), fontSize: 14,fontFamily:'Poppins',fontWeight: FontWeight.bold),
              children: <TextSpan>[
                TextSpan(text: ' Sign in',
                    style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 1).withOpacity(0.9), fontSize: 14,fontFamily:'Poppins',fontWeight: FontWeight.bold),
                        recognizer: TapGestureRecognizer()
                        ..onTap = () async {
                          _isLoading = await Navigator.push(context,
                              MaterialPageRoute(builder: (context) => Login()));
                        },
                )
              ]
          ),
        ),
      )
      );

    final description = Padding(
    padding: EdgeInsets.all(10),
    child: RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam gravida magna nec enim aliquet, vel congue enim aliquam. Donec id congue sapien.',
        style: TextStyle(color: Color.fromRGBO(255, 255, 255, 1).withOpacity(0.6), fontSize: 14,fontFamily:'Poppins',height: 1.5)
      ),
    ),
      );


    return Scaffold(
          backgroundColor:color,
          body: Container(
            padding: EdgeInsets.all(40.0),
            child:ListView(children: <Widget>[
              avatar,
              Container(
                margin: EdgeInsets.only(left: 30, right:30, top: 30),
                height: 72,
                width: 315,
                child:Center(child:Text(
                'Benefits of Gemini 1',
                style: TextStyle(fontSize: 26,fontFamily:'Poppins',color:Colors.white),                
              ))), 
              description,
              Container(
                  margin: const EdgeInsets.only(top: 20.0,bottom: 20.0),
                child:Wrap(
                  // mainAxisAlignment: MainAxisAlignment.center,
                    spacing: 10,
                    alignment: WrapAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                      onTap: () {Navigator.push(context,MaterialPageRoute(
                          builder: (context)=>Signup()
                          ));},
                      child: ClipOval(
                        child: Container(
                          color:Colors.black12,
                          height: 14.0,
                          width: 14.0,
                        ),
                      ),
                    ),
                      GestureDetector(
                      onTap: () {Navigator.push(context,MaterialPageRoute(
                          builder: (context)=>Secondpage()
                          ));},
                      child: ClipOval(
                        child: Container(
                          color: Colors.white,
                          height: 14.0,
                          width: 14.0,
                        ),
                      ),
                    ),
                      GestureDetector(
                      onTap: () {Navigator.push(context,MaterialPageRoute(
                          builder: (context)=>Thirdpage()
                          ));},
                      child: ClipOval(
                        child: Container(
                          color:Colors.black12,
                            height: 14.0,
                          width: 14.0,
                        ),
                      ),
                    ),
                      GestureDetector(
                      onTap: () {Navigator.push(context,MaterialPageRoute(
                          builder: (context)=>Fourthpage()
                          ));},
                      child: ClipOval(
                        child: Container(
                          color:Colors.black12,
                          height: 14.0,
                          width: 14.0,
                        ),
                      ),
                    ),

                    ]
                ),
              ),
            signup,
            login
      ])),

    );
  }
}

class Thirdpage extends StatelessWidget
{  
  @override
  Widget build(BuildContext context)
  {
     final avatar = Container(
          margin: EdgeInsets.all(20),
          width: 120,
          height: 120,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              image: NetworkImage('https://googleflutter.com/sample_image.jpg'),
              fit: BoxFit.fill
            ),
          ),
    );
    
    // Padding(
    //   padding: EdgeInsets.all(20),
    //     child: Hero(
    //     tag: 'logo',
    //     child: SizedBox(
    //       height: 160,
    //       child: Image.asset('homescreen.png'),
    //     )
    //   ),
    // );

    final login = Container(            
            margin: const EdgeInsets.only(top: 20.0),
            padding: EdgeInsets.all(10),
            child: Center(
              child: RichText(
                text: TextSpan(
                    text: 'Already have an account?',
                    style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 1).withOpacity(0.6),fontFamily:'Poppins',fontSize: 14,fontWeight: FontWeight.bold),
                    children: <TextSpan>[
                      TextSpan(text: ' Sign in',
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1).withOpacity(0.9),fontFamily:'Poppins',fontSize: 14,fontWeight: FontWeight.bold),
                              recognizer: TapGestureRecognizer()
                              ..onTap = () async {
                                _isLoading = await Navigator.push(context,
                                    MaterialPageRoute(builder: (context) => Login()));
                              },
                      )
                    ]
                ),
              ),
            )
            );

     
    final description = Padding(
      padding: EdgeInsets.all(10),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: 'Gavida magna nec enim aliquet, vel congue enim aliquam. Donec id congue sapien. Praesent molestie volutpat nisi, ac convallis ligula egestas ut.',
          style: TextStyle(color: Color.fromRGBO(255, 255, 255, 1).withOpacity(0.6), fontSize: 14,fontFamily:'Poppins',height: 1.5)
        ),
      ),
      );

     final signup = Material(
          borderRadius: BorderRadius.circular(30.0),
          color: Color.fromRGBO(255, 154, 66, 1),
          child: MaterialButton(
          height: 40.0,
          minWidth: 60.0,
          padding: EdgeInsets.fromLTRB(95.0, 18.0, 95.0, 18.0),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => Signup(),
                fullscreenDialog: true),
            );
          },
          textColor: color,
          child: Text("SIGN UP",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14),textAlign: TextAlign.center),
          ),
      );
    return Scaffold(

         backgroundColor:color,
            body: Container(
              padding: EdgeInsets.all(40.0),
              child:ListView(children: <Widget>[
                avatar,
                Container(
                  margin: EdgeInsets.only(left: 30, right:30, top: 30),
                  height: 72,
                  width: 315,
                  child:Center(child:Text(
                  'Benefits of Gemini 2',
                  style: TextStyle(fontSize: 26,fontFamily:'Poppins',color:Colors.white),
                  
                ))), 
               description,
                Container(
                  margin: const EdgeInsets.only(top: 20.0,bottom: 20.0),
                  child:Wrap(
                    // mainAxisAlignment: MainAxisAlignment.center,
                      spacing: 10,
                      alignment: WrapAlignment.center,                 
                      children: <Widget>[
                        GestureDetector(
                        onTap: () {Navigator.push(context,MaterialPageRoute(
                            builder: (context)=>Signup()
                            ));},
                        child: ClipOval(
                          child: Container(
                            color:Colors.black12,
                            height: 14.0,
                            width: 14.0,
                          ),
                        ),
                      ),
                       GestureDetector(
                        onTap: () {Navigator.push(context,MaterialPageRoute(
                            builder: (context)=>Secondpage()
                            ));},
                        child: ClipOval(
                          child: Container(
                            color:Colors.black12,
                            height: 14.0,
                            width: 14.0,
                          ),
                        ),
                      ),
                       GestureDetector(
                        onTap: () {Navigator.push(context,MaterialPageRoute(
                            builder: (context)=>Thirdpage()
                            ));},
                        child: ClipOval(
                          child: Container(
                            color: Colors.white,
                             height: 14.0,
                            width: 14.0,
                          ),
                        ),
                      ),
                       GestureDetector(
                        onTap: () {Navigator.push(context,MaterialPageRoute(
                            builder: (context)=>Fourthpage()
                            ));},
                        child: ClipOval(
                          child: Container(
                            color: Colors.black,
                            height: 14.0,
                            width: 14.0,
                          ),
                        ),
                      ),

                      ]
                  ),
                ),
                 signup,
                 login, 
       ])),
    );
  }
}

class Fourthpage extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    final avatar = Container(
          margin: EdgeInsets.all(20),
          width: 120,
          height: 120,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              image: NetworkImage('https://googleflutter.com/sample_image.jpg'),
              fit: BoxFit.fill
            ),
          ),
    );
    final login = Container(            
      margin: const EdgeInsets.only(top: 20.0),
      padding: EdgeInsets.all(10),
      child: Center(
        child: RichText(
          text: TextSpan(
              text: 'Already have an account?',
              style: TextStyle(
                  color: Color.fromRGBO(255, 255, 255, 1).withOpacity(0.6), fontSize: 14,fontFamily:'Poppins',fontWeight: FontWeight.bold),
              children: <TextSpan>[
                TextSpan(text: ' Sign in',
                    style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 1).withOpacity(0.9),fontFamily:'Poppins',fontSize: 14,fontWeight: FontWeight.bold),
                        recognizer: TapGestureRecognizer()
                        ..onTap = () async {
                          _isLoading = await Navigator.push(context,
                              MaterialPageRoute(builder: (context) => Login()));
                        },
                )
              ]
          ),
        ),
      )
    );

    final signup = Material(
      borderRadius: BorderRadius.circular(30.0),
      color: Color.fromRGBO(255, 154, 66, 1),
      child: MaterialButton(
      height: 40.0,
      minWidth: 60.0,
      padding: EdgeInsets.fromLTRB(95.0, 18.0, 95.0, 18.0),
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Signup(),
            fullscreenDialog: true),
        );
      },
      textColor: color,
      child: Text("SIGN UP",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14),textAlign: TextAlign.center),
      ),
    );

    final description = Padding(
      padding: EdgeInsets.all(10),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: 'Gavida magna nec enim aliquet, vel congue enim aliquam. Donec id congue sapien. Praesent molestie volutpat nisi, ac convallis ligula egestas ut.',
          style: TextStyle(color: Color.fromRGBO(255, 255, 255, 1).withOpacity(0.6),fontFamily:'Poppins',fontSize: 14,height: 1.5)
        ),
      ),
    );

    return Scaffold(
          backgroundColor:color,
            body: Container(
              padding: EdgeInsets.all(40.0),
              child:ListView(children: <Widget>[
                avatar,
                Container(
                  margin: EdgeInsets.only(left: 30, right:30, top: 30),
                  height: 72,
                  width: 315,
                  child:Center(child:Text(
                  'Benefits of Gemini 3',
                  style: TextStyle(fontSize: 26,fontFamily:'Poppins',color:Colors.white),
                ))), 
                description,
                Container(
                   margin: const EdgeInsets.only(top: 20.0,bottom: 20.0),
                  child:Wrap(
                    // mainAxisAlignment: MainAxisAlignment.center,
                       spacing: 10,
                  alignment: WrapAlignment.center,
                 
                      children: <Widget>[
                        GestureDetector(
                        onTap: () { Navigator.push(context,MaterialPageRoute(
                            builder: (context)=>Signup()
                            ));},
                        child: ClipOval(
                          child: Container(
                            color: Colors.black,
                            height: 14.0,
                            width: 14.0,
                          ),
                        ),
                      ),
                       GestureDetector(
                        onTap: () { Navigator.push(context,MaterialPageRoute(
                            builder: (context)=>Secondpage()
                            ));},
                        child: ClipOval(
                          child: Container(
                            color: Colors.black,
                            height: 14.0,
                            width: 14.0,
                          ),
                        ),
                      ),
                       GestureDetector(
                        onTap: () { Navigator.push(context,MaterialPageRoute(
                            builder: (context)=>Thirdpage()
                            ));},
                        child: ClipOval(
                          child: Container(
                            color: Colors.black,
                             height: 14.0,
                            width: 14.0,
                          ),
                        ),
                      ),
                       GestureDetector(
                        onTap: () { Navigator.push(context,MaterialPageRoute(
                            builder: (context)=>Fourthpage()
                            ));},
                        child: ClipOval(
                          child: Container(
                            color: Colors.white,
                            height: 14.0,
                            width: 14.0,
                          ),
                        ),
                      ),

                      ]
                  ),
                ),
           signup,
           login,                
       ])),
    );
  }
}