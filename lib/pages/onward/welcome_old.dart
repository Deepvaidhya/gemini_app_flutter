import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:gemini_app/pages/login/login_screen.dart';
import 'package:gemini_app/pages/signup/signup.dart';

class Welcome extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _WelcomeState();
  }
}

class _WelcomeState extends State<Welcome> {
  BuildContext _ctx;  

  final color = const  Color.fromRGBO(38, 62, 114, 1);
  @override
  bool _isLoading = false;
  Widget build(BuildContext context) {
    // TODO: implement build
    _ctx = context;
    
    final avatar = Padding(
      padding: EdgeInsets.all(20),
        child: Hero(
        tag: 'logo',
        child: SizedBox(
          height: 160,
          child: Image.asset('homescreen.png'),
        )
      ),
    );

    
    final description = Padding(
      padding: EdgeInsets.all(10),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam gravida magna nec enim aliquet, vel congue enim aliquam. Donec id congue sapien. Praesent molestie volutpat nisi, ac convallis ligula egestas ut. Proin eget rutrum turpis, eu sodales lectus. Vestibulum volutpat felis ligula, sed sagittis nibh feugiat at.',
          style: TextStyle(color: Color.fromRGBO(255, 255, 255, 1).withOpacity(0.6), fontSize: 14)
        ),
      ),
    );

    final start = Material(
      elevation: 8.0,      
      borderRadius: BorderRadius.circular(30.0),
      color: Color.fromRGBO(255, 154, 66, 1),//Color(0xFFF57F17),
      child: MaterialButton(
      padding: EdgeInsets.fromLTRB(85.0, 20.0, 85.0, 20.0),
      height: 45.0,
      minWidth: 100.0,
      //minWidth: MediaQuery.of(context).size.width,
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Signup(),
            fullscreenDialog: true),
        );
      },
      textColor: color,
      child: Text("SIGN UP",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14),textAlign: TextAlign.center),
      ),
    );

    return new Scaffold(  
      backgroundColor: color,   
      body: new Center(
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [            
          avatar,
          new Text(
            "Welcome to Gemini",
            style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white,height: 5,fontSize: 26),
          ), 
          new Padding(
            padding: const EdgeInsets.all(8.0),
            child: description,
          ),
          start,
          Container(            
            margin: const EdgeInsets.only(top: 20.0),
            padding: EdgeInsets.all(10),
            child: Center(
              child: RichText(
                text: TextSpan(
                    text: 'Already have an account?',
                    style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 1).withOpacity(0.6), fontSize: 14,fontWeight: FontWeight.bold),
                    children: <TextSpan>[
                      TextSpan(text: ' Sign in',
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1).withOpacity(0.9), fontSize: 14,fontWeight: FontWeight.bold),
                              recognizer: TapGestureRecognizer()
                              ..onTap = () async {
                                _isLoading = await Navigator.push(context,
                                    MaterialPageRoute(builder: (context) => Login()));
                              },
                      )
                    ]
                ),
              ),
            )
            ),
          ],
        ),
      ),
    );
  }

  
}
