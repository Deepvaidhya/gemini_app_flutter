import 'package:flutter/material.dart';
import 'package:gemini_app/pages/check-in/mental_screen.dart';

class StartCheckinScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _StartCheckinScreenState();
  }
}

class _StartCheckinScreenState extends State<StartCheckinScreen> {
  BuildContext _ctx;

  final txtcolor = const Color.fromRGBO(38, 62, 114, 1);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    _ctx = context;

    final avatar = Padding(
      padding: EdgeInsets.all(20),
        child: Hero(
        tag: 'logo',
        child: SizedBox(
          height: 160,
          child: Image.asset('homescreen.png'),
        )
      ),
    );

    
    final description = Padding(
      padding: EdgeInsets.all(20.0),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: 'You will be asked a few questions about your health and well-being. It will only take about 5 minutes.',
          style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1).withOpacity(0.9), fontFamily:'Poppins',height: 1.5,fontSize: 14)
        ),
      ),
    );

    final next = Material(
     elevation: 8.0,     
      borderRadius: BorderRadius.circular(30.0),
      color: Color.fromRGBO(255, 154, 66, 1),
      child: MaterialButton(
      padding: EdgeInsets.fromLTRB(95.0, 18.0, 95.0, 18.0),
      height: 40.0,
      minWidth: 60.0,
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => MentalScreen(),
              fullscreenDialog: true),
        );
      },
      textColor: txtcolor,
      child: Text("NEXT", style: TextStyle(fontWeight: FontWeight.bold),textAlign: TextAlign.center),
      ),
    );

    return new Scaffold(
      appBar: new AppBar(
        leading: IconButton(
          icon: new Image.asset('left-arrow.png'),
          tooltip: 'Back',
          onPressed: () => Navigator.of(context).pop(),
        ), 
        centerTitle: true,
      ),
      body: new Center(
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [            
          avatar,
          Container(
            margin: const EdgeInsets.only(top: 8.0),
            padding: EdgeInsets.all(40.0),
            child: new Text(
              "Now let’s start your health check-in",
              style: TextStyle(fontWeight: FontWeight.bold,color: txtcolor,fontSize: 26,fontFamily:'Poppins'),textAlign: TextAlign.center,
            ),
          ), 
          Container(
            margin: const EdgeInsets.only(top: 2.0),
            child: new Padding(
              padding: const EdgeInsets.all(8.0),
              child: description,
            ),
          ),
          next
          ],
        ),
      ),
    );
  }
}
