import 'package:flutter/material.dart';
import 'package:gemini_app/pages/dashboard/dashboard.dart';

class ImgvThanks extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _ImgvThanksState();
  }
}

class _ImgvThanksState extends State<ImgvThanks> {
  BuildContext _ctx;

  final txtcolor = const Color.fromRGBO(38, 62, 114, 1);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    _ctx = context;

    final start = Material(
      elevation: 8.0,      
      borderRadius: BorderRadius.circular(30.0),
      color: Theme.of(context).accentColor,
      child: MaterialButton(
      padding: EdgeInsets.fromLTRB(45.0, 18.0, 45.0, 18.0),
      height: 40.0,
      minWidth: 60.0,
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => Dashboard(),
              fullscreenDialog: true),
        );
      },
      textColor: txtcolor,
      child: Text("BACK TO DASHBOARD", style: TextStyle(fontWeight: FontWeight.bold),textAlign: TextAlign.center),
      ),
    );

    return new Scaffold(
       appBar: new AppBar(
        leading: IconButton(
          icon: new Image.asset('left-arrow.png'),
          tooltip: 'Back',
          onPressed: () => Navigator.of(context).pop(),
        ), 
        title: Text("IMGV HEALTH CHECK-IN",
        style: new TextStyle(
            fontWeight: FontWeight.bold,
            color: txtcolor,fontFamily:'Poppins',fontSize: 12
          ),
          textAlign: TextAlign.center),
        centerTitle: true,
      ),
      body: new Center(
        child: Column(
          children: <Widget> [ 
          Container(
            margin: const EdgeInsets.only(top: 60.0),
            padding: EdgeInsets.all(40.0),
            child: new Text(
              "Thank you!",
              style: TextStyle(fontWeight: FontWeight.bold,color: Color.fromRGBO(38, 62, 114, 1),fontSize: 26,fontFamily:'Poppins'),textAlign: TextAlign.center,
            ),
          ), 
          Container(
            margin: const EdgeInsets.only(top: 60.0),
            child: start,
          )
          ],
        ),
      ),
    );    
  }
}
