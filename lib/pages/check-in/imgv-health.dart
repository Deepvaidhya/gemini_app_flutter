import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:gemini_app/pages/check-in/imgv-thanks.dart';
class ImgvHealth extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return new _ImgvHealthState();
  }
}


class _ImgvHealthState extends State<ImgvHealth> {

  BuildContext _ctx; 

  bool _isLoading = false;
  
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  String _weight;


  final color = const Color(0xffECEFF1);

  @override
  Widget build(BuildContext context) {
    _ctx = context;
    String _dropDownValue;

    var submit = new Column(
      children: <Widget>[
        new Form(
          key: formKey,
          child: Container(
            margin: const EdgeInsets.only(bottom: 100.0),
            child: new Column(
              children: <Widget>[
               new TextFormField(
                    obscureText: true,
                    onSaved: (val) => _weight = val,
                    validator: (val) {
                      return val.length < 1
                          ? "Please enter"
                          : null;
                    },
                    decoration: new InputDecoration(
                       labelStyle: TextStyle(
                          color: Color.fromRGBO(153, 153, 153, 1),
                          fontSize: 16
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Color.fromRGBO(153, 153, 153, 1), width: 3.0),
                      ),                      
                    ),
                  ),
              ],
            ),
          ),
        ),
      ],
      crossAxisAlignment: CrossAxisAlignment.center,
    );
   
    var dropdownBtn = new DropdownButton<String>(
        icon: Icon(Icons.keyboard_arrow_down),
        isExpanded: true,
        iconSize: 24,
        elevation: 16,
        style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1)),
        underline: Container(
          height: 1,
          width: 60,
          color: Color.fromRGBO(102, 102, 102, 1)
        ),        
        items: <String>['10', '20', '30', '40'].map((String value) {
          return new DropdownMenuItem<String>(
            value: value,
            child: new Text(
              value,              
              style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1)),
            ),
            
          );
        }).toList(),
        hint: _dropDownValue == null
        ? Text('Please enter')
        : Text(
        _dropDownValue,
        style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1)),
        ),
        onChanged: (_) {},
      );

    final next = Container(
      margin: const EdgeInsets.only(left: 250.0,top: 30.0,right: 2.0,bottom: 2.0),
      child: Padding(
        padding: EdgeInsets.fromLTRB(4, 0, 10, 0),
      child : Text.rich(
            TextSpan(
              text: 'NEXT',
              style: TextStyle(fontFamily:'Poppins',color:Color.fromRGBO(102, 102, 102, 1),fontSize: 14
              ),
              recognizer: TapGestureRecognizer()
                ..onTap = () async {
                  _isLoading = await Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ImgvThanks()));
                },
              children: <InlineSpan>[
                WidgetSpan(
                  child: Container(          
                    padding: EdgeInsets.only(top: 5.0,left: 10.0),
                    child: new Image.asset('right-arrow.png')
                  )
                ),
              ],
            ),
          ),
      )
    );

    return new Scaffold(
      appBar: new AppBar(
        leading: IconButton(
          icon: new Image.asset('left-arrow.png'),
          tooltip: 'Back',
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("IMGV HEALTH Check-In".toUpperCase(),
        style: new TextStyle(
            fontWeight: FontWeight.bold,
            color: Color.fromRGBO(38, 62, 114, 1),fontFamily:'Poppins',fontSize: 12
        ), textAlign: TextAlign.center),
        centerTitle: true,
      ), 
      body: Container(
        color: Colors.white,
        margin: const EdgeInsets.only(top: 30.0),
        child: new Center(
          child: Column(    
            children: <Widget> [
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: new Text(
                "What is your weight today?",
                style: TextStyle(fontWeight: FontWeight.bold,color: Color.fromRGBO(38, 62, 114, 1),fontSize: 20,fontFamily:'Poppins'),textAlign: TextAlign.center,
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 60.0),
              padding: EdgeInsets.all(10.0),
              child: dropdownBtn,
            ),
            Container(
              margin: const EdgeInsets.only(top: 30.0),
              padding: EdgeInsets.only(top: 18.0),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: next,
              ),
            ),         
            ],
          ),
        ),
      ),
    );
  }
  
}



