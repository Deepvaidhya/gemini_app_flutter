import 'package:flutter/material.dart';
import 'package:gemini_app/pages/check-in/imgv-health.dart';

class HealthAward extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _HealthAwardState();
  }
}

class _HealthAwardState extends State<HealthAward> {
  BuildContext _ctx;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    _ctx = context;

    final avatar = Container(
          margin: EdgeInsets.all(20),
          width: 150,
          height: 150,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              image: NetworkImage('https://googleflutter.com/sample_image.jpg'),
              fit: BoxFit.fill
            ),
          ),
    );

    
    final description = Padding(
      padding: EdgeInsets.all(10),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: 'You’ve earned a trowel for doing your health check-in this week! Keep it up and you’ll soon have a grown a new flower for your garden!',
          style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1).withOpacity(0.7), fontFamily:'Poppins',fontSize: 14,height: 1.5)
        ),
      ),
    );

    final start = Material(
      elevation: 8.0,      
      borderRadius: BorderRadius.circular(30.0),
      color: Theme.of(context).accentColor,
      child: MaterialButton(
      padding: EdgeInsets.fromLTRB(95.0, 18.0, 95.0, 18.0),
      height: 40.0,
      minWidth: 60.0,
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ImgvHealth(),
              fullscreenDialog: true),
        );
      },
      textColor: Color.fromRGBO(38, 62, 114, 1),
      child: Text("Go to dashboard", style: TextStyle(fontWeight: FontWeight.bold,fontFamily:'Poppins'),textAlign: TextAlign.center),
      ),
    );

    return new Scaffold(
      appBar: new AppBar(
        leading: IconButton(
          icon: new Image.asset('left-arrow.png'),
          tooltip: 'Back',
          onPressed: () => Navigator.of(context).pop(),
        ),
      ), 
      body: new Center(
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [            
          avatar,
          Container(
            margin: const EdgeInsets.only(top: 8.0),
            padding: EdgeInsets.all(40.0),
            child: new Text(
              "Well done - you’ve earned an award!!",
              style: TextStyle(fontWeight: FontWeight.bold,color: Color.fromRGBO(38, 62, 114, 1),fontSize: 26,fontFamily:'Poppins'),textAlign: TextAlign.center,
            ),
          ), 
          Container(
            margin: const EdgeInsets.only(top: 5.0),
            child: new Padding(
              padding: const EdgeInsets.all(30.0),
              child: description,
            ),
          ),
          start,
          ],
        ),
      ),
    );
  }

  
}
