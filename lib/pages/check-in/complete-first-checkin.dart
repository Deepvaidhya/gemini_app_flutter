import 'package:flutter/material.dart';
import 'package:gemini_app/pages/check-in/checkin-navigation.dart';

class CompleteFirstcheckin extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _CompleteFirstcheckinState();
  }
}

class _CompleteFirstcheckinState extends State<CompleteFirstcheckin> {
  BuildContext _ctx;

  final txtcolor = const Color.fromRGBO(38, 62, 114, 1);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    _ctx = context;

    
    final description = Padding(
      padding: EdgeInsets.all(4),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: 'The is the first step in helping your garden to grow!',
          style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1).withOpacity(0.7), fontSize: 14)
        ),
      ),
    );

    final start = Material(
      elevation: 8.0,      
      borderRadius: BorderRadius.circular(30.0),
      color: Theme.of(context).accentColor,
      child: MaterialButton(
      padding: EdgeInsets.fromLTRB(95.0, 18.0, 95.0, 18.0),
      height: 40.0,
      minWidth: 60.0,
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => CheckinNavigation(),
              fullscreenDialog: true),
        );
      },
      textColor: txtcolor,
      child: Text("OKAY", style: TextStyle(fontWeight: FontWeight.bold),textAlign: TextAlign.center),
      ),
    );

    return new Scaffold(
       appBar: new AppBar(
        leading: IconButton(
          icon: new Image.asset('left-arrow.png'),
          tooltip: 'Back',
          onPressed: () => Navigator.of(context).pop(),
        ), 
        centerTitle: true,
      ),
      body: new Center(
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [ 
          Container(
            margin: const EdgeInsets.only(top: 5.0),
            padding: EdgeInsets.all(30.0),
            child: new Text(
              "Thanks for completing your first check in!",
              style: TextStyle(fontWeight: FontWeight.bold,color: Color.fromRGBO(38, 62, 114, 1),fontSize: 20,fontFamily:'Poppins'),textAlign: TextAlign.center,
            ),
          ), 
          new Padding(
            padding: const EdgeInsets.all(5.0),
            child: description,
          ),
          Container(
            margin: const EdgeInsets.only(top: 10.0),
            padding: EdgeInsets.all(8.0),
            child: start,
          ),
          ],
        ),
      ),
    );
    
  }

  }
