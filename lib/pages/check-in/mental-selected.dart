import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:gemini_app/pages/check-in/physical-health.dart';
import 'package:demoji/demoji.dart';

class MentalSelected extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _MentalSelectedState();
  }
}

class _MentalSelectedState extends State<MentalSelected> {
  BuildContext _ctx;
  bool _isLoading = false;
  double _value = 0;  
  final txtcolor = const Color.fromRGBO(38, 62, 114, 1);  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    _ctx = context;

    final avatar = Padding(
      padding: EdgeInsets.all(10),
        child: Hero(
        tag: 'cry',
        child: SizedBox(
          height: 100,
          child: Image.asset('cry/cry.png'),
        )
      ),
    );

    final avatar1 = Padding(
      padding: EdgeInsets.all(10),
        child: Hero(
        tag: 'angry',
        child: SizedBox(
          height: 100,
          child: Image.asset('angry/1.png'),
        )
      ),
    );

    final avatar2 = Padding(
      padding: EdgeInsets.all(10),
        child: Hero(
        tag: 'laugh',
        child: SizedBox(
          height: 100,
          child: Image.asset('laugh/laugh1.png'),
        )
      ),
    );
    
    final description = Padding(
      padding: EdgeInsets.all(15),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: 'Please slide the round green button below to the left or right to select how you feel.',
          style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1).withOpacity(0.9), fontSize: 12)
        ),
      ),
    );
    
    final next = Container(
      margin: const EdgeInsets.only(left: 250.0,top: 2.0,right: 2.0,bottom: 2.0),
      child: Padding(
        padding: EdgeInsets.fromLTRB(4, 0, 10, 0),
      child : Text.rich(
            TextSpan(
              text: 'NEXT',
              style: TextStyle(
                fontWeight: FontWeight.bold,fontFamily:'Poppins',color:Color.fromRGBO(102, 102, 102, 1)
              ),
              recognizer: TapGestureRecognizer()
                ..onTap = () async {
                  _isLoading = await Navigator.push(context,
                      MaterialPageRoute(builder: (context) => PhysicalHealth()));
                },
              children: <InlineSpan>[
                WidgetSpan(
                  child: Container(          
                   padding: EdgeInsets.only(top: 5.0,left: 18.0),
                    child: new Image.asset('right-arrow.png')
                  )
                ),
              ],
            ),
          ),
      )
    );

    return new Scaffold(
      appBar: new AppBar(
        leading: IconButton(
          //icon: Icon(Icons.arrow_back, color: Colors.black),
          icon: new Image.asset('left-arrow.png'),
          tooltip: 'Back',
          onPressed: () => Navigator.of(context).pop(),
        ), 
        title: Text("MENTAL HEALTH",
        style: new TextStyle(
            fontWeight: FontWeight.bold,
            color: txtcolor,fontSize: 12
          ),
          textAlign: TextAlign.center),
        centerTitle: true,
      ),
      body: new Center(
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            new Text(
              "How are you feeling today emotionally?",
              style: TextStyle(fontWeight: FontWeight.bold,color: txtcolor,fontSize: 20),textAlign: TextAlign.center,
            ),             
            Container(
              margin: const EdgeInsets.only(top: 2.0),
              child: new Padding(
                padding: const EdgeInsets.all(8.0),
                child: description,
              ),
            ),
          avatar,
          new Text(
              "« Really bad »",
              style: TextStyle(fontWeight: FontWeight.bold,color: txtcolor,fontSize: 20),textAlign: TextAlign.center,
          ), 
          avatar1,
          Container(
            constraints: BoxConstraints(minWidth: 100, maxWidth: 200),
            padding: EdgeInsets.all(10),
            child: SliderTheme(
              data: SliderTheme.of(context).copyWith(
                activeTrackColor: Color.fromRGBO(53, 123, 64, 1),
                inactiveTrackColor: Color.fromRGBO(235, 235, 235, 1),
                trackShape: RectangularSliderTrackShape(),
                trackHeight: 4.0,
                thumbColor: Color.fromRGBO(53, 123, 64, 1),
                thumbShape: RoundSliderThumbShape(enabledThumbRadius: 12.0),
                overlayColor: Color.fromRGBO(235, 235, 235, 1).withAlpha(32),
                overlayShape: RoundSliderOverlayShape(overlayRadius: 28.0),
              ),
              child: Slider(
                value: _value,
                onChanged: (_newValue) => setState(() {
                  _value = _newValue;
                }),
                max: 5,
                min: 0,
                divisions: 5,
                label: _emojify(),
              ),
            ),
          ),
          avatar2,
          next
          ],
        ),
      ),
    );
  }
  String _emojify() {
    switch (_value.floor()) {
      case 0:
        return "« Really bad »";//Demoji.angry;
      case 1:
        return Demoji.neutral_face;
      case 2:
        return Demoji.sweat_smile;
      case 3:
        return Demoji.smiley;
      case 4:
        return Demoji.star_struck;
      case 5:
        return "« Really comfortable »";//Demoji.heart_eyes;
    }

    return '';
  }
}
