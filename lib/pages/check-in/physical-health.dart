import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:gemini_app/pages/check-in/physical-selected.dart';

class PhysicalHealth extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _PhysicalHealthState();
  }
}
BuildContext _ctx;
bool _isLoading = false;
class _PhysicalHealthState extends State<PhysicalHealth> {
  
  final txtcolor = const Color.fromRGBO(38, 62, 114, 1);  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    _ctx = context;

    final avatar = Padding(
      padding: EdgeInsets.all(10),
        child: Hero(
        tag: 'neutral',
        child: SizedBox(
          height: 100,
          child: Image.asset('neutral/neutral.png'),
        )
      ),
    );

    final avatar1 = Padding(
      padding: EdgeInsets.all(10),
        child: Hero(
        tag: 'angry',
        child: SizedBox(
          height: 100,
          child: Image.asset('angry/1.png'),
        )
      ),
    );

    final avatar2 = Padding(
      padding: EdgeInsets.all(10),
        child: Hero(
        tag: 'laugh',
        child: SizedBox(
          height: 100,
          child: Image.asset('laugh/laugh1.png'),
        )
      ),
    );
    
    final description = Padding(
      padding: EdgeInsets.all(15),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: 'Please slide the round green button below to the left or right to select how you feel.',
          style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1).withOpacity(0.9), fontSize: 12)
        ),
      ),
    );

    final next1 = Material(
      child: MaterialButton(
      padding: EdgeInsets.fromLTRB(65.0, 18.0, 65.0, 18.0),
      height: 40.0,
      minWidth: 60.0,
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PhysicalSelected(),
              fullscreenDialog: true),
        );
      },
      textColor: Color.fromRGBO(102, 102, 102, 1),
      child: Text("NEXT",style: TextStyle(fontWeight: FontWeight.bold),textAlign: TextAlign.right),
      ),
    );

    final next = Container(
      margin: const EdgeInsets.only(left: 250.0,top: 2.0,right: 2.0,bottom: 2.0),
      child: Padding(
        padding: EdgeInsets.fromLTRB(4, 0, 10, 0),
        child : Text.rich(
              TextSpan(
                text: 'NEXT',
                style: TextStyle(
                  fontWeight: FontWeight.bold, color:Color.fromRGBO(102, 102, 102, 1)
                ),
                recognizer: TapGestureRecognizer()
                  ..onTap = () async {
                    _isLoading = await Navigator.push(context,
                        MaterialPageRoute(builder: (context) => PhysicalSelected()));
                  },
                children: <InlineSpan>[
                  WidgetSpan(
                    child: Container(          
                      padding: EdgeInsets.only(top: 6.0,left: 18.0),
                      child: new Image.asset('right-arrow.png')
                    )
                  ),
                ],
              ),
        ),
      )
    );


    return new Scaffold(
      appBar: new AppBar(
        leading: IconButton(
          //icon: Icon(Icons.arrow_back, color: Colors.black),
          icon: new Image.asset('left-arrow.png'),
          tooltip: 'Back',
          onPressed: () => Navigator.of(context).pop(),
        ), 
        title: Text("MENTAL HEALTH",
        style: new TextStyle(
            fontWeight: FontWeight.bold,
            color: txtcolor,fontSize: 12
          ),
          textAlign: TextAlign.center),
        centerTitle: true,
      ),
      body: new Center(
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            new Text(
              "How are you feeling today physically?",
              style: TextStyle(fontWeight: FontWeight.bold,color: txtcolor,fontSize: 20),textAlign: TextAlign.center,
            ),             
            Container(
              margin: const EdgeInsets.only(top: 2.0),
              child: new Padding(
                padding: const EdgeInsets.all(8.0),
                child: description,
              ),
            ),
          avatar,
          avatar1,
          avatar2,
          next
          ],
        ),
      ),
    );
  }
}
