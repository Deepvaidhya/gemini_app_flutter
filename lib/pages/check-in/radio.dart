import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:gemini_app/pages/check-in/complete-first-checkin.dart';

class Meds extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _MedsState();
  }
}
enum SingingCharacter { yes, no }

class _MedsState extends State<Meds> {
   SingingCharacter _character = SingingCharacter.yes;

   bool _hasBeenPressed = false;
   bool _hasnotBeenPressed = false;
   BuildContext _ctx;
   bool _isLoading = false;
    
  
  final txtcolor = const Color.fromRGBO(38, 62, 114, 1);  
  @override

  Widget build(BuildContext context) {
    
    _ctx = context;
    var yesbtn = Container(
      child: Row(children:<Widget>[
        ListTile(
          title: const Text('Yes'),
          leading: Radio(
            value: SingingCharacter.yes,
            groupValue: _character,
            onChanged: (SingingCharacter value) {
              setState(() {
                _character = value;
              });
            },
          ),
        ),
        ListTile(
          title: const Text('No'),
          leading: Radio(
            value: SingingCharacter.no,
            groupValue: _character,
            onChanged: (SingingCharacter value) {
              setState(() {
                _character = value;
              });
            },
          ),
        ),
        ],
        ),
    );

    final yesbtn1 = RaisedButton(        
        shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(30.0))),
        color: _hasBeenPressed ? txtcolor : Colors.white.withOpacity(0),
          onPressed: () => {
            setState(() {
              _hasBeenPressed = !_hasBeenPressed;
            })
          },
        textColor: Colors.white,
        padding: EdgeInsets.fromLTRB(45.0, 20.0, 45.0, 20.0),        
        child: Padding(
          padding: EdgeInsets.fromLTRB(0,0,0,0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[        
              Container(
                padding: EdgeInsets.fromLTRB(6, 4, 4, 4),
                child: Text('Yes', 
                style: TextStyle(fontWeight: FontWeight.bold,color: _hasBeenPressed ? Colors.white :Color.fromRGBO(51, 51, 51, 1),fontSize: 16),textAlign: TextAlign.left),
              ),
              
              Container(
                margin: const EdgeInsets.only(left: 250.0,top: 2.0,right: 2.0,bottom: 2.0),
                child: Padding(
                  padding: EdgeInsets.fromLTRB(4, 0, 10, 0),
                  child: Icon(
                        Icons.done,color:Colors.white,size: 20,
                  ),
                ),
              ),
              ],
              
          )
        )
    );

    final nobtn = RaisedButton(
        shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(30.0))),
        color: _hasnotBeenPressed ? txtcolor : Color(0x00000000),
          onPressed: () => {
            setState(() {
              _hasnotBeenPressed = !_hasnotBeenPressed;
            })
          },
        textColor: Colors.white,
        padding: EdgeInsets.fromLTRB(45.0, 20.0, 45.0, 20.0),        
        child: Padding(
          padding: EdgeInsets.fromLTRB(0,0,0,0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[        
              Container(
                //color: txtcolor,
                padding: EdgeInsets.fromLTRB(6, 4, 4, 4),
                child: Text('No', 
                style: TextStyle(fontWeight: FontWeight.bold,color: _hasnotBeenPressed ? Colors.white :Color.fromRGBO(51, 51, 51, 1),fontSize: 16),textAlign: TextAlign.left),
              ),
            
              Container(
                margin: const EdgeInsets.only(left: 250.0,top: 2.0,right: 2.0,bottom: 2.0),
                child: Padding(
                  padding: EdgeInsets.fromLTRB(4, 0, 10, 0),
                  child: Icon(
                        Icons.done,color:Colors.white,size: 20,
                  ),
                ),
              ),
              ],
              
          )
        )
    );

    
    final description = Padding(
      padding: EdgeInsets.all(15),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: 'Please select one of the choices below',
          style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1).withOpacity(0.9), fontSize: 12)
        ),
      ),
    );
    

    final next = Container(
      margin: const EdgeInsets.only(left: 300.0,top: 40.0,right: 2.0,bottom: 2.0),
      child: Padding(
        padding: EdgeInsets.fromLTRB(4, 0, 10, 0),
      child : Text.rich(
            TextSpan(
              text: 'NEXT',
              style: TextStyle(
                fontWeight: FontWeight.bold,fontFamily:'Poppins',color:Color.fromRGBO(102, 102, 102, 1)
              ),
              recognizer: TapGestureRecognizer()
                ..onTap = () async {
                  _isLoading = await Navigator.push(context,
                      MaterialPageRoute(builder: (context) => CompleteFirstcheckin()));
                },
              children: <InlineSpan>[
                WidgetSpan(
                  child: Container(   
                    margin: const EdgeInsets.only(left: 10.0),       
                    padding: EdgeInsets.only(top: 2.0,left: 0.0),
                    child: new Image.asset('right-arrow.png')
                  )
                ),
              ],
            ),
          ),
      )
    );

    return new Scaffold(
      appBar: new AppBar(
        leading: IconButton(
          icon: new Image.asset('left-arrow.png'),
          tooltip: 'Back',
          onPressed: () => Navigator.of(context).pop(),
        ), 
        title: Text("MEDICATIONS / SUPPLEMENTS",
        style: new TextStyle(
            fontWeight: FontWeight.bold,
            color: txtcolor,fontSize: 12
          ),
          textAlign: TextAlign.center),
        centerTitle: true,
      ),
      body: new Center(
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [ 
            new Text(
              "Did you take your medications or supplements today?",
              style: TextStyle(fontWeight: FontWeight.bold,color: txtcolor,fontSize: 20),textAlign: TextAlign.center,
            ),
          Container(
            margin: const EdgeInsets.only(top: 2.0),
            child: new Padding(
              padding: const EdgeInsets.all(8.0),
              child: description,
            ),
          ),
          yesbtn,
          Padding(
              padding: EdgeInsets.only(top: 8),
              child: nobtn
          ),
          next
          ],
        ),
      ),
    );
  }
}


class CustomRadio extends StatefulWidget {
  @override
  createState() {
    return new CustomRadioState();
  }
}

class CustomRadioState extends State<CustomRadio> {
  List<RadioModel> sampleData = new List<RadioModel>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sampleData.add(new RadioModel(false, 'A', 'April 18'));
    sampleData.add(new RadioModel(false, 'B', 'April 17'));
    sampleData.add(new RadioModel(false, 'C', 'April 16'));
    sampleData.add(new RadioModel(false, 'D', 'April 15'));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("ListItem"),
      ),
      body: new ListView.builder(
        itemCount: sampleData.length,
        itemBuilder: (BuildContext context, int index) {
          return new InkWell(
            //highlightColor: Colors.red,
            splashColor: Colors.blueAccent,
            onTap: () {
              setState(() {
                sampleData.forEach((element) => element.isSelected = false);
                sampleData[index].isSelected = true;
              });
            },
            child: new RadioItem(sampleData[index]),
          );
        },
      ),
    );
  }
}

class RadioItem extends StatelessWidget {
  final RadioModel _item;
  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: new EdgeInsets.all(15.0),
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Container(
            height: 50.0,
            width: 50.0,
            child: new Center(
              child: new Text(_item.buttonText,
                  style: new TextStyle(
                      color:
                          _item.isSelected ? Colors.white : Colors.black,
                      //fontWeight: FontWeight.bold,
                      fontSize: 18.0)),
            ),
            decoration: new BoxDecoration(
              color: _item.isSelected
                  ? Colors.blueAccent
                  : Colors.transparent,
              border: new Border.all(
                  width: 1.0,
                  color: _item.isSelected
                      ? Colors.blueAccent
                      : Colors.grey),
              borderRadius: const BorderRadius.all(const Radius.circular(2.0)),
            ),
          ),
          new Container(
            margin: new EdgeInsets.only(left: 10.0),
            child: new Text(_item.text),
          )
        ],
      ),
    );
  }
}

class RadioModel {
  bool isSelected;
  final String buttonText;
  final String text;

  RadioModel(this.isSelected, this.buttonText, this.text);
}