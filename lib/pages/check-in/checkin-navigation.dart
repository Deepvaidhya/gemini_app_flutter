import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:gemini_app/pages/check-in/health-award.dart';

class CheckinNavigation extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _CheckinNavigationState();
  }
}

class _CheckinNavigationState extends State<CheckinNavigation> {
    BuildContext _ctx;
    bool _hasBeenPressed = false;
    bool _hasnotBeenPressed = false;
    bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    _ctx = context;

     final myImageAndCaption = [
      ["graphics/icons/bloodpressure_3063097/bloodpressure_3063097.png",'mental Health'],
      ["graphics/icons/physical_452710/physical_452710.png",'physical Health'],
      ["graphics/icons/bloodpressure_3063097/bloodpressure_3063097.png",'Blood Pressure'],
      ["graphics/icons/pulse_2996766/pulse_2996766.png",'pulse'],      
      ["graphics/icons/weight_372597/weight_372597.png",'Weight'],
      ["graphics/icons/medication_2937361/medication_2937361.png",'medications/ Supplements'],
    ]; 

    final description = Padding(
      padding: EdgeInsets.all(25),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: 'Please select one of the below to complete your health check-in',
          style: TextStyle(color: Colors.white, fontFamily:'Poppins',fontSize: 10,height: 1.5)
        ),
      ),
    );

    return new Scaffold(
      backgroundColor: Color.fromRGBO(38, 62, 114, 1),
      appBar: new AppBar(
        leading: IconButton(
          icon: new Image.asset('left-arrow.png'),
          tooltip: 'Back',
          onPressed: () => Navigator.of(context).pop(),
        ),
        
      ), 
      body: new Center(
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
          Container(
            margin: const EdgeInsets.only(top: 10.0),
            child: new Text(
              "HEALTH CHECK-IN",
              style: TextStyle(fontWeight: FontWeight.bold,fontFamily:'Poppins',color: Colors.white,fontSize: 14),
            ),
          ), 
         
          Container(
            margin: const EdgeInsets.only(top: 5.0),
            child: new Padding(
              padding: const EdgeInsets.all(30.0),
              child: description,
            ),
          ),

        Flexible(            
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: GridView.count(
            crossAxisCount: 3,
            children: [
              ...myImageAndCaption.map(
                (i) => Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Center(
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(100.0)),
                        elevation: 5,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Image.asset(
                            i.first,
                            fit: BoxFit.fitWidth,
                            width: 40,
                            height: 40,                  
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        alignment: Alignment.bottomCenter,
                        child: Text(
                          i.last.toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold,fontFamily:'Poppins',color: Colors.white,fontSize: 10),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              
            ],
            ),
          )),
          Container(
            margin: const EdgeInsets.only(bottom: 30.0),
            child: ClipOval(
              child: Material(
                color:Colors.white,
                child: InkWell(
                  splashColor: Colors.transparent, // inkwell color
                  child: SizedBox(width: 40, height: 40, child: Icon(Icons.clear,color: Color.fromRGBO(38, 62, 114, 1))),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => HealthAward()),
                    );
                  },
                ),
              ),
            ),
          ),
          ],
        ),
        
        
      ),
    );
  }

  

  
}
