import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:gemini_app/pages/check-in/complete-first-checkin.dart';

class Meds extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _MedsState();
  }
}

class _MedsState extends State<Meds> {
  List<RadioModel> sampleData = new List<RadioModel>();
  bool _isLoading = false;
  var  txtcolor = const Color.fromRGBO(38, 62, 114, 1);  

  @override
  void initState() {
    super.initState();
    sampleData.add(new RadioModel(false, 'Yes', ''));
    sampleData.add(new RadioModel(false, 'No', ''));
  }

  @override
  Widget build(BuildContext context) {    

    final btn = Expanded(
      child: Center(
        child: new ListView.builder(
        itemCount: sampleData.length,
        itemBuilder: (BuildContext context, int index) {
          return new InkWell(
            splashColor: Color.fromRGBO(38, 62, 114, 1),
            onTap: () {
              setState(() {
                sampleData.forEach((element) => element.isSelected = false);
                sampleData[index].isSelected = true;
              });
            },
            child: new RadioItem(sampleData[index]),
          );
        },
        ),
      ),
    );

    final next = Container(
      margin: const EdgeInsets.only(left: 250.0,top: 0.0,right: 0.0,bottom: 80.0),
      padding: EdgeInsets.fromLTRB(4, 0, 10, 0),
      child: Text.rich(
            TextSpan(
              text: 'NEXT',
              style: TextStyle(
                fontWeight: FontWeight.bold,fontFamily:'Poppins',color:Color.fromRGBO(102, 102, 102, 1)
              ),
              recognizer: TapGestureRecognizer()
                ..onTap = () async {
                  _isLoading = await Navigator.push(context,
                      MaterialPageRoute(builder: (context) => CompleteFirstcheckin()));
                },
              children: <InlineSpan>[
                WidgetSpan(
                  child: Container(          
                    padding: EdgeInsets.only(top: 5.0,left: 18.0),
                    child: new Image.asset('right-arrow.png')
                  )
                ),
              ],
            ),
          )
    );

    return new Scaffold(
      appBar: new AppBar(
        leading: IconButton(
          icon: new Image.asset('left-arrow.png'),
          tooltip: 'Back',
          onPressed: () => Navigator.of(context).pop(),
        ), 
        title: Text("MEDICATIONS / SUPPLEMENTS",
        style: new TextStyle(
            fontWeight: FontWeight.bold,
            color: txtcolor,fontFamily:'Poppins',fontSize: 12
        ),
        textAlign: TextAlign.center),
        centerTitle: true,
      ),
      body: Center(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(top: 40.0),
              padding: const EdgeInsets.all(18.0),
              child: new Text(
                  "Did you take your medications or supplements today?",
                  style: TextStyle(fontWeight: FontWeight.bold,fontFamily:'Poppins',color: txtcolor,fontSize: 20),textAlign: TextAlign.center,
                ),
            ),
            Container(
              padding: const EdgeInsets.all(14.0),
              child: new Text(
                  "Please select one of the choices below",
                  style: TextStyle(fontWeight: FontWeight.bold,fontFamily:'Poppins',fontSize: 12,color: Color.fromRGBO(51, 51, 51, 1).withOpacity(0.9)),textAlign: TextAlign.center,
                ),
            ),
            btn,            
            next
          ],
        ),
      )
    );

  }
}

class RadioItem extends StatelessWidget {
  final RadioModel _item;
  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: new EdgeInsets.all(30.0),
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Center(
            child: new Container(
              padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
               height: 55.0,
               width: 0.5*MediaQuery.of(context).size.width,
              child: new Center(
                child: new Text(_item.buttonText,
                    style: new TextStyle(
                        color: _item.isSelected ? Colors.white : Colors.black,fontSize: 18.0)),
              ),
              decoration: new BoxDecoration(
                color: _item.isSelected
                    ? const Color.fromRGBO(38, 62, 114, 1)
                    : Colors.transparent,
                border: new Border.all(
                    width: 1.0,
                    color: _item.isSelected
                        ? const Color.fromRGBO(38, 62, 114, 1)
                        : Colors.transparent),
                borderRadius: const BorderRadius.all(const Radius.circular(30.0)),
              ),
            ),
            
          ),
          new Container(
            margin: new EdgeInsets.only(left: 10.0),
            child: new Center(
              child: new Text(_item.text,style: TextStyle(fontFamily:'Poppins',fontWeight: FontWeight.bold,fontSize: 16),textAlign: TextAlign.left))
              
            ),
        ],
      ),
    );
  }
}

class RadioModel {
  bool isSelected;
  final String buttonText;
  final String text;

  RadioModel(this.isSelected, this.buttonText, this.text);
}