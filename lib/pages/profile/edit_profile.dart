import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';
import 'package:gemini_app/pages/profile/profile.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditProfile extends StatefulWidget {
  EditProfileState createState() => EditProfileState();
}

class EditProfileState extends State<EditProfile> {
  BuildContext _ctx; 

  bool isLoggedIn = false;
  String name = '';

  @override
  void initState() {
    super.initState();
  }

  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  String _email, _fname, _lname, _password;

  void _submit() {
    final form = formKey.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
    }
  }
 
  final color = const Color(0xffECEFF1);

  @override
  Widget build(BuildContext context) {
    
    _ctx = context;

    final avatar = new Stack(
          children: <Widget>[
            Container(
            margin: const EdgeInsets.only(top:0.0,bottom:10.0),
            padding: const EdgeInsets.only(top:0.0,bottom:2.0),
            width: 100,
            height: 100,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                image: NetworkImage('https://googleflutter.com/sample_image.jpg'),
                fit: BoxFit.fill
              ),
            ),
          ),
          new Positioned(
            right: 0.0,
            bottom: 0.0,        
            child: Container(
              color: Color.fromRGBO(53, 123, 64, 1),
              child: new IconButton(
                  icon: new Icon(Icons.edit,color: Colors.white,size: 20,)
                  ,onPressed: (){Profile();}
              ),
            ),
          ),
          ],
        );

    final avatar1 = Container(
      child: Padding(
        padding: const EdgeInsets.all(30.0),      
        child: Icon(Icons.edit,color:Colors.white,size: 20),
      ),
      margin: const EdgeInsets.only(top:0.0,bottom:10.0),
      padding: const EdgeInsets.only(top:0.0,bottom:2.0),
      width: 100,
      height: 100,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(
          image: NetworkImage('https://googleflutter.com/sample_image.jpg'),
          fit: BoxFit.fill
        ),
      ),
    );

    final editbtn = Material(
      elevation: 8.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Theme.of(context).accentColor,
      child: MaterialButton(
      height: 40.0,
      minWidth: 60.0,
      padding: EdgeInsets.fromLTRB(95.0, 18.0, 95.0, 18.0),
      onPressed: _submit,
      textColor: Color.fromRGBO(38, 62, 114, 1),
      child: Text("save".toUpperCase(), style: TextStyle(fontWeight: FontWeight.bold),textAlign: TextAlign.center),
      ),
    );

    final logoutbtn = Material(
        elevation: 8.0,      
        borderRadius: BorderRadius.circular(30.0),
        color: Color.fromRGBO(38, 62, 114, 1),
        child: MaterialButton(
        padding: EdgeInsets.fromLTRB(95.0, 18.0, 95.0, 18.0),
        height: 40.0,
        minWidth: 60.0,
        onPressed: logout,
        textColor: Colors.white,
        child: Text("LOGOUT", style: TextStyle(fontWeight: FontWeight.bold),textAlign: TextAlign.center),
      ),
    );

    var editForm = new Column(
      children: <Widget>[
        new Form(
          key: formKey,
          child: Container(
            margin: const EdgeInsets.only(bottom: 80.0),
            padding: const EdgeInsets.all(8.0),
            child: new Column(
              children: <Widget>[
                 new TextFormField(
                    onSaved: (val) => _fname = val,
                    validator: (val) {
                      if (val.isEmpty) {
                        return 'Please enter a valid fname';
                      }
                      return null;
                    },
                    style: new TextStyle(
                      fontFamily: "Poppins",
                      color: Color.fromRGBO(51,51,51,1)
                    ),
                    decoration: InputDecoration(
                    labelText: 'First Name',
                    focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color.fromRGBO(51,51,51,1)),                      
                    ),
                    labelStyle: TextStyle(
                        color: Color.fromRGBO(153, 153, 153, 1),
                        fontSize: 14
                    ),
                    ),
                    ),
                 new TextFormField(
                    onSaved: (val) => _lname = val,
                    validator: (val) {
                      if (val.isEmpty) {
                        return 'Please enter a valid lname';
                      }
                      return null;
                    },
                    style: new TextStyle(
                      fontFamily: "Poppins",
                      color: Color.fromRGBO(51,51,51,1)
                    ),
                    decoration: new InputDecoration(
                      focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Color.fromRGBO(51,51,51,1)),                      
                      ),
                      labelStyle: TextStyle(
                          color: Color.fromRGBO(153, 153, 153, 1),
                          fontSize: 14
                      ),
                    labelText: 'Last Name',
                    ),
                  ),
                new TextFormField(
                    onSaved: (val) => _email = val,
                    validator: (val) {
                      return !EmailValidator.validate(val, true)
                          ? "Please enter a valid email"
                          : null;
                    },
                    keyboardType: TextInputType.emailAddress,
                    style: new TextStyle(
                      fontFamily: "Poppins",
                      color: Color.fromRGBO(51,51,51,1)
                    ),
                    decoration: new InputDecoration(
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Color.fromRGBO(51,51,51,1)),                      
                      ),
                      labelStyle: TextStyle(
                          color: Color.fromRGBO(153, 153, 153, 1),
                          fontSize: 14
                      ),
                      labelText: "Email address",
                    ),
                  ),
                new TextFormField(
                    obscureText: true,
                    onSaved: (val) => _password = val,
                    validator: (val) {
                      return val.length < 1
                          ? "Please enter current password"
                          : null;
                    },
                    style: new TextStyle(
                      fontFamily: "Poppins",
                      color: Color.fromRGBO(51,51,51,1)
                    ),
                    decoration: new InputDecoration(
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Color.fromRGBO(51,51,51,1)),                      
                      ),
                      labelStyle: TextStyle(
                          color: Color.fromRGBO(153, 153, 153, 1),
                          fontSize: 14
                      ),
                      labelText: 'Current Password *',
                    ),
                  ),
                  new TextFormField(
                    obscureText: true,
                    onSaved: (val) => _password = val,
                    validator: (val) {
                      return val.length < 1
                          ? "Please enter new password"
                          : null;
                    },
                    style: new TextStyle(
                      fontFamily: "Poppins",
                      color: Color.fromRGBO(51,51,51,1)
                    ),
                    decoration: new InputDecoration(
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Color.fromRGBO(51,51,51,1)),                      
                      ),
                      labelStyle: TextStyle(
                          color: Color.fromRGBO(153, 153, 153, 1),
                          fontSize: 14
                      ),
                      labelText: 'New Password (8+ characters)',
                    ),
                  ),
                  new TextFormField(
                    obscureText: true,
                    onSaved: (val) => _password = val,
                    validator: (val) {
                      return val.length < 1
                          ? "Please enter confirm password"
                          : null;
                    },
                    style: new TextStyle(
                      fontFamily: "Poppins",
                      color: Color.fromRGBO(51,51,51,1)
                    ),
                    decoration: new InputDecoration(
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Color.fromRGBO(51,51,51,1)),                      
                      ),
                      labelStyle: TextStyle(
                          color: Color.fromRGBO(153, 153, 153, 1),
                          fontSize: 14
                      ),
                      labelText: 'Confirm Password',
                    ),
                  ),
                 
              ],
            ),
          ),
        ),

        _isLoading ? new CircularProgressIndicator(  backgroundColor: color, valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),) : editbtn
      ],
      crossAxisAlignment: CrossAxisAlignment.center,
    );
    
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: new Image.asset('left-arrow-w.png'),
            tooltip: 'Back',
            onPressed: () => Navigator.of(context).pop(),
        ), 
        title: Text("public profile".toUpperCase(), style: TextStyle(fontSize: 12, color: Colors.white,fontFamily:'Poppins')), 
        elevation: 18.0, 
        centerTitle: true,
        backgroundColor:Color.fromRGBO(38, 62, 114, 1),
        actions: [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(Icons.email,color: Colors.white,),            
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(Icons.notifications,color: Colors.white,),
          ),
        ],
      ),
      key: scaffoldKey,
      backgroundColor: color,
      body: new Center(
      child: Column(
        children: <Widget>[                 
          new Text(
            "Edit your account",
            style: TextStyle(fontWeight: FontWeight.bold,color: Color.fromRGBO(38, 62, 114, 1),fontFamily: 'Poopins',height: 5, fontSize: 18),
          ), 
          avatar,
          editForm,            
          Padding(
            padding: EdgeInsets.only(top: 8),
            child: logoutbtn
          )
        ],
      ),
    )
    );
  }
  Future<Null> logout() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('username', null);
    Navigator.of(_ctx).pushReplacementNamed("/");
  }
}


