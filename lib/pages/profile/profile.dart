import 'package:flutter/material.dart';
import 'package:gemini_app/pages/onward/welcome.dart';
import 'package:image_picker_web/image_picker_web.dart';
import 'package:gemini_app/pages/check-in/start-checkin_screen.dart';

import 'dart:html' as html;
import 'dart:typed_data';
import 'dart:async';
import 'dart:convert';
import 'package:http_parser/http_parser.dart';
import 'package:http/http.dart' as http;

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {

  BuildContext _ctx;
  Image pickedImage;
  Image image;
  final txtcolor = const Color.fromRGBO(38, 62, 114, 1);

  @override
  void initState() {
    super.initState();
  }

  pickImage() async {
    Image fromPicker = await ImagePickerWeb.getImage(outputType: ImageType.widget);

    if (fromPicker != null) {
      setState(() {
        pickedImage = fromPicker;
        startWebFilePicker();
      });
    }
  }

  List<int> _selectedFile;
  Uint8List _bytesData;
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  startWebFilePicker() async {
    html.InputElement uploadInput = html.FileUploadInputElement();
    uploadInput.multiple = false;
    uploadInput.draggable = true;
    uploadInput.click();

    uploadInput.onChange.listen((e) {
      final files = uploadInput.files;
      final file = files[0];
      final reader = new html.FileReader();

      reader.onLoadEnd.listen((e) {
        _handleResult(reader.result);
      });
      reader.readAsDataUrl(file);
    });
  }

  void _handleResult(Object result) {
    setState(() {
      _bytesData = Base64Decoder().convert(result.toString().split(",").last);
      _selectedFile = _bytesData;
    });
  }

  Future<String> makeRequest() async {
    var url = Uri.parse(
        "http://localhost/flutter_test/upload_image.php");
    var request = new http.MultipartRequest("POST", url);
    request.files.add(await http.MultipartFile.fromBytes('file', _selectedFile,
        contentType: new MediaType('application', 'multipart/form-data'),
        filename: "file"));

    request.send().then((response) {
      if (response.statusCode == 200) print("Uploaded!");
    });

    showDialog(
        barrierDismissible: false,
        context: context,
        child: new AlertDialog(
          title: new Text("Details"),
          //content: new Text("Hello World"),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: <Widget>[
                new Text("Upload successfull"),
              ],
            ),
          ),
          actions: <Widget>[
            new FlatButton(
              child: new Text('cancel'.toUpperCase()),
              onPressed: () {
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => Profile()),
                  (Route<dynamic> route) => false,
                );
              },
            ),
          ],
        ));
  }

  

  @override
  Widget build(BuildContext context) {
    _ctx = context;
        
    final avatar = Container(
          margin: EdgeInsets.all(20),
          width: 120,
          height: 120,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              image: NetworkImage('https://googleflutter.com/sample_image.jpg'),
              fit: BoxFit.fill
            ),
          ),
    );

    final next = Material(
     elevation: 8.0,     
      borderRadius: BorderRadius.circular(30.0),
      color: txtcolor,
      child: MaterialButton(
      padding: EdgeInsets.fromLTRB(65.0, 18.0, 65.0, 18.0),
      height: 40.0,
      minWidth: 60.0,
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => StartCheckinScreen(),
              fullscreenDialog: true),
        );
      },
      textColor: Color.fromRGBO(251, 251, 251, 1),
      child: Text("next".toUpperCase(), style: TextStyle(fontWeight: FontWeight.bold),textAlign: TextAlign.center),
      ),
    );

    
    final description = Padding(
      padding: EdgeInsets.all(8.0),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: 'Other users in the GEMINI community\nwill be able to see this photo.',
          style: TextStyle(color: Color.fromRGBO(51, 51, 51, 1).withOpacity(0.7),fontFamily:'Poppins',fontSize: 14)
        ),
      ),
    );

    final upload = Material(
      elevation: 8.0,     
      borderRadius: BorderRadius.circular(30.0),
      color: Theme.of(context).accentColor,
      child: MaterialButton(
      padding: EdgeInsets.fromLTRB(65.0, 18.0, 65.0, 18.0),
      height: 40.0,
      minWidth: 60.0,
      onPressed: () => pickImage(),
      textColor: Color.fromRGBO(38, 62, 114, 1),
      child: Text("Upload a photo".toUpperCase(), style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,fontFamily:'Poppins',),textAlign: TextAlign.center),
      ),
    );

    final useupload =  Form(
      autovalidate: true,
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.only(top: 5.0, left: 28),
        child: new SingleChildScrollView(
        child: new Container(
            width: 350,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[                    
                  Center(
                    child: MaterialButton(
                      padding: EdgeInsets.fromLTRB(65.0, 18.0, 65.0, 18.0),
                      color: Theme.of(context).accentColor,
                      elevation: 8,
                      highlightElevation: 2,
                      shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                      textColor: Color.fromRGBO(38, 62, 114, 1),child: Text("Upload a photo".toUpperCase(), style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,fontFamily:'Poppins',),textAlign: TextAlign.center),
                      onPressed: () {
                        pickImage();
                      },
                    ),
                  ),
                  Divider(
                    color: Colors.teal,
                  ),
                  Center(
                    child: RaisedButton(
                      padding: EdgeInsets.fromLTRB(65.0, 18.0, 65.0, 18.0),
                      color: Theme.of(context).accentColor,
                      elevation: 8,
                      highlightElevation: 2,
                      textColor: Color.fromRGBO(38, 62, 114, 1),
                      shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                      onPressed: () {
                        makeRequest();
                      },
                      child: Text("use this photo".toUpperCase(), style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,fontFamily:'Poppins',),textAlign: TextAlign.center),
                    ),
                  ),
                ])),)
      ),
    );

    void _navigateToNextScreen(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => StartCheckinScreen()));
  }

    return new Scaffold(
      appBar: new AppBar(
        leading: IconButton(
          icon: new Image.asset('left-arrow.png'),
          tooltip: 'Back',
          onPressed: () => Navigator.of(context).pop(),
        ),         
        centerTitle: true,
        actions: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: RaisedButton( 
            color: Color(0xFFFFFFFF),       
            child: Text('Skip this step',
            style: TextStyle(color: Color.fromRGBO(153, 153, 153, 1),fontFamily:'Poppins',fontSize: 14),textAlign: TextAlign.left),
            onPressed: () {_navigateToNextScreen(context);},
            ), 
          ),
        ]
      ),
      body: new Center(
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
                  InkWell(
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 150.0,
                        child: CircleAvatar(
                          radius: 85.0,
                          child: ClipOval(
                            child: (pickedImage != null)
                            ? pickedImage
                            : avatar,
                          ),
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                  ),  
                  new Text(
                    "Upload a photo of yourself\n for your profile",
                    style: TextStyle(fontWeight: FontWeight.bold,color: Color.fromRGBO(38, 62, 114, 1),fontFamily:'Poppins',fontSize: 20),textAlign: TextAlign.center,
                  ),

                  new Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: description,
                  ),
                  useupload,                                    
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: next,
                  ), 
                  
                ],
        ),
      ),
    );

  }
}