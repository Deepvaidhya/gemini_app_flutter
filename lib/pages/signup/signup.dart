import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/gestures.dart';
import 'package:gemini_app/pages/login/login_screen.dart';

class Signup extends StatefulWidget {
  SignupState createState() => SignupState();
}

class SignupState extends State<Signup> {
  var _controller = TextEditingController();
  BuildContext _ctx; 

  bool isLoggedIn = false;
  String name = '';

  @override
  void initState() {
    super.initState();
  }

  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  String _password, _username, _fname, _lname;

  void _submit() {
    final form = formKey.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
    }
  }
 
  final color = const Color(0xffECEFF1);

  @override
  Widget build(BuildContext context) {
    
    _ctx = context;

    final signupButon = Material(
      elevation: 8.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Theme.of(context).accentColor,
      child: MaterialButton(
      height: 40.0,
      minWidth: 60.0,
      //minWidth: MediaQuery.of(context).size.width,
      padding: EdgeInsets.fromLTRB(65.0, 18.0, 65.0, 18.0),
      onPressed: _submit,
      textColor: Color.fromRGBO(38, 62, 114, 1),
      child: Text("GET STARTED", style: TextStyle(fontWeight: FontWeight.bold),textAlign: TextAlign.center),
      ),
    );

    var signupForm = new Column(
      children: <Widget>[
        new Form(          
          key: formKey,
          //onChanged: () => setState(() => signupButon = formKey.currentState.validate()),
          child: Container(
            margin: const EdgeInsets.only(bottom: 80.0),
            padding: const EdgeInsets.all(8.0),
            child: new Column(
              children: <Widget>[
                
                // new TextFormField(
                //   controller: _controller,
                //   onSaved: (val) => _fname = val,
                //   validator: (val) {
                //     if (val.isEmpty) {
                //       return 'Please enter a valid fname';
                //     }
                //     return null;
                //   },
                //   decoration: InputDecoration(
                //     labelText: 'First Name',
                //     focusedBorder: UnderlineInputBorder(
                //       borderSide: BorderSide(color: Color.fromRGBO(51,51,51,1)),                      
                //     ),
                //     labelStyle: TextStyle(
                //         color: Color.fromRGBO(153, 153, 153, 1),
                //         fontSize: 16
                //     ),
                    
                    
                //     suffixIcon: IconButton(
                //         icon: Icon(
                //           Icons.clear,
                //           color: Colors.red ??Icon(Icons.done,color: Color.fromRGBO(0, 0, 0, 1)),
                //         ),
                //         splashColor: Colors.redAccent,
                //         onPressed: () {
                //           _controller.clear();
                //         }),
                //   ),
                // ),
                 new TextFormField(
                    onSaved: (val) => _fname = val,
                    validator: (val) {
                      if (val.isEmpty) {
                        return 'Please enter a valid fname';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                    suffixIcon: Icon(Icons.done,color: Color.fromRGBO(0, 0, 0, 1),)??Icon(Icons.done),
                    labelText: 'First Name',
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Color.fromRGBO(51,51,51,1)),                      
                    ),
                    labelStyle: TextStyle(
                        color: Color.fromRGBO(153, 153, 153, 1),
                        fontSize: 16
                    ),
                    ),
                    ),
                 new TextFormField(
                    onSaved: (val) => _lname = val,
                    validator: (val) {
                      if (val.isEmpty) {
                        return 'Please enter a valid lname';
                      }
                      return null;
                    },
                    style: new TextStyle(
                      fontFamily: "Poppins",
                    ),
                    decoration: new InputDecoration(                      
                    labelText: 'Last Name',
                    //suffixIcon: Icon(Icons.done,color: Color.fromRGBO(0, 0, 0, 1),)??Icon(Icons.done),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Color.fromRGBO(51,51,51,1)),                      
                    ),
                    labelStyle: TextStyle(
                        color: Color.fromRGBO(153, 153, 153, 1),
                        fontSize: 16
                    ),
                    ),
                  ),
                new TextFormField(
                    onSaved: (val) => _username = val,
                    validator: (val) {
                      return !EmailValidator.validate(val, true)
                          ? "Please enter a valid email"
                          : null;
                    },
                    keyboardType: TextInputType.emailAddress,
                    style: new TextStyle(
                      fontFamily: "Poppins",
                    ),
                    decoration: new InputDecoration(                       
                      labelText: 'Email address',
                      //suffixIcon: Icon(Icons.done,color: Color.fromRGBO(0, 0, 0, 1),)??Icon(Icons.done),
                       focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Color.fromRGBO(51,51,51,1)),                      
                      ),
                      labelStyle: TextStyle(
                          color: Color.fromRGBO(153, 153, 153, 1),
                          fontSize: 16
                      ),
                    ),
                  ),
                new TextFormField(
                    obscureText: true,
                    onSaved: (val) => _password = val,
                    validator: (val) {
                      return val.length < 1
                          ? "Please enter password"
                          : null;
                    },
                    style: new TextStyle(
                      fontFamily: "Poppins",
                      color: Color.fromRGBO(153, 153, 153, 1),
                    ),
                    decoration: new InputDecoration(                       
                      labelText: 'Password (8+ characters)',
                      //suffixIcon: Icon(Icons.done,color: Color.fromRGBO(0, 0, 0, 1),)??Icon(Icons.done),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Color.fromRGBO(51,51,51,1)),                      
                      ),
                      labelStyle: TextStyle(
                          color: Color.fromRGBO(153, 153, 153, 1),
                          fontSize: 16
                      ),
                    ),
                  ),
                 
              ],
            ),
          ),
        ),

        

         _isLoading ? new CircularProgressIndicator(  backgroundColor: color, valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),) : signupButon
      ],
      crossAxisAlignment: CrossAxisAlignment.center,
    );
    
    return Scaffold(
        appBar: null,
        key: scaffoldKey,
        backgroundColor: color,
        body: SafeArea(
          child: ListView(
            padding: const EdgeInsets.all(36.0),
            children: <Widget> [
              SizedBox(height: 60.0,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                    "Create your account",
                    style: TextStyle(fontWeight: FontWeight.bold,color: Color.fromRGBO(38, 62, 114, 1),fontFamily: 'Poopins',height: 5, fontSize: 26),
                  ), 
                  // SizedBox(
                  //   height: 155.0,
                  //   child: Image.asset(
                  //     "homescreen.png",
                  //     fit: BoxFit.contain,
                  //   ),
                  // ),
                  signupForm,
                  
                  Container(
                    margin: const EdgeInsets.only(top: 20.0),
                    padding: EdgeInsets.all(10),
                    child: Center(
                      child: RichText(
                        text: TextSpan(
                            text: 'Already have an account?',
                            style: TextStyle(
                                color: Colors.grey, fontSize: 14,fontWeight: FontWeight.bold),
                            children: <TextSpan>[
                              TextSpan(text: ' Sign in',
                                  style: TextStyle(
                                      color: Color.fromRGBO(51, 51, 51, 1).withOpacity(0.8), fontSize: 16,fontWeight: FontWeight.bold),
                                      recognizer: TapGestureRecognizer()
                                      ..onTap = () async {
                                        _isLoading = await Navigator.push(context,
                                            MaterialPageRoute(builder: (context) => Login()));
                                      },
                              )
                            ]
                        ),
                      ),
                    )
                  ),
                  //  FlatButton(
                  //   child: Text(
                  //     'Don’t remember your password??'
                  //   ),
                  //   onPressed: () {
                  //     Navigator.push(
                  //       context,
                  //       MaterialPageRoute(
                  //           builder: (context) => CreateAccount(),
                  //           fullscreenDialog: true),
                  //     );
                  //   },
                  // ),
                  
                ],
              ),
            ],
          ),
        ));
  }
}


