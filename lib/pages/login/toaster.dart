import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Function toast(
      String msg, Toast toast, ToastGravity toastGravity, Color colors) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: toast,
        gravity: toastGravity,
        timeInSecForIosWeb: 1,
        backgroundColor: colors,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flutter Toast"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Demo on Flutter Toast Message Plugin"),
            SizedBox(
              height: 20.0,
            ),
            MaterialButton(
              elevation: 5.0,
              height: 50.0,
              minWidth: 100,
              color: Colors.blueAccent,
              textColor: Colors.white,
              child: Text("Show Toast at Bottom"),
              onPressed: () {
                toast(
                    "This is Demo 1 Toast at BOTTOM",
                    Toast.LENGTH_LONG,
                    ToastGravity.BOTTOM,
                    Colors.green); // calling a function to show Toast message
              },
            ),
            SizedBox(
              height: 20.0,
            ),
            MaterialButton(
              elevation: 5.0,
              height: 50.0,
              minWidth: 100,
              color: Colors.redAccent,
              textColor: Colors.white,
              child: Text("Show Toast at TOP"),
              onPressed: () {
                toast(
                    "This is Demo 2 Toast at top",
                    Toast.LENGTH_SHORT,
                    ToastGravity.TOP,
                    Colors.red); // calling a function to show Toast message
              },
            ),
          ],
        ),
      ),
    );
  }
}