import 'package:gemini_app/models/auth.dart';
import 'package:gemini_app/models/login_model.dart';
//import 'package:gemini_app/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class LoginContract {
  void onLoginSuccess(LoginModel response);

  void onLoginError(String errorTxt);
}

class LoginPresenter {
  LoginContract _view;
  RestDatasource api = new RestDatasource();

  LoginPresenter(this._view);

  doLogin(String username, String password) async{
    try
    {
      LoginModel response= await api.login(username, password);
      _view.onLoginSuccess(response);
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('username', username);
    }
    catch (e)
    {
      _view.onLoginError(e.toString().substring(e.toString().indexOf(":")+1));

    }
  }
}
