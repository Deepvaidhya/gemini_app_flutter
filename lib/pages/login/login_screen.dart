import 'dart:ui';

import 'package:gemini_app/models/login_model.dart';
import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';
import 'package:gemini_app/pages/login/login_screen_presenter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/gestures.dart';
import 'package:gemini_app/pages/home/home.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Login extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return new _LoginState();
  }
}

class _LoginState extends State<Login> implements LoginContract {

  BuildContext _ctx; 

  bool isLoggedIn = false;
  bool loginState = false;
  String name = '';
  bool _isLoading = false;
  String _username, _password;

  final color = const Color(0xffECEFF1);
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>(); 

  @override
  void initState() {
    super.initState();
    autoLogIn();
  }

  void autoLogIn() async {

    Function toast(String msg, Toast toast, ToastGravity toastGravity, Color colors) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: toast,
      gravity: toastGravity,
      timeInSecForIosWeb: 1,
      backgroundColor: colors,
      textColor: Colors.white,
      fontSize: 14.0,
      webShowClose: true
    );
    }
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String userId = prefs.getString('username');
    if (userId != null) {
      setState(() {
        isLoggedIn = true;
        name = userId;       
      });
      toast(
        "Login Success",
        Toast.LENGTH_LONG,
        ToastGravity.BOTTOM,
        Colors.green
      );
      
      Navigator.of(_ctx).pushReplacementNamed("/reminder");
      return;
    }
  } 

  

  LoginPresenter _presenter;
  _LoginState() {
    _presenter = new LoginPresenter(this);
  }  

  void _submit() async {
    final form = formKey.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
      _presenter.doLogin(_username, _password);
    }    
    
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  } 

  @override
  Widget build(BuildContext context) {
    _ctx = context;

    final loginButon = Material(
      elevation: 8.0,      
      borderRadius: BorderRadius.circular(30.0),
      color: Theme.of(context).accentColor,
      child: MaterialButton(
      padding: EdgeInsets.fromLTRB(95.0, 18.0, 95.0, 18.0),
      height: 40.0,
      minWidth: 60.0,
      onPressed: _submit,
      textColor: Color.fromRGBO(38, 62, 114, 1),
      child: Text("LOG IN", style: TextStyle(fontWeight: FontWeight.bold,fontFamily:'Poppins'),textAlign: TextAlign.center),
      ),
    );

    var loginForm = new Column(
      children: <Widget>[
        new Form(
          key: formKey,
          child: Container(
            margin: const EdgeInsets.only(bottom: 100.0),
            child: new Column(
              children: <Widget>[
               new TextFormField(
                  onSaved: (val) => _username = val,
                  validator: (val) {
                    return !EmailValidator.validate(val, true)
                        ? "Please enter a valid email"
                        : null;
                  },
                  decoration: InputDecoration(
                    labelText: "Username or Email address",
                    labelStyle: TextStyle(
                        color: Color.fromRGBO(153, 153, 153, 1),
                        fontSize: 16
                    ),
                    focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color.fromRGBO(51,51,51,1)),                      
                    ),
                  ),
                  ), 
                new TextFormField(
                  obscureText: true,
                  onSaved: (val) => _password = val,
                  validator: (val) {
                    return val.length < 1
                        ? "Please enter password"
                        : null;
                  },
                   decoration: new InputDecoration(
                      labelText: "Password",
                      labelStyle: TextStyle(
                        color: Color.fromRGBO(153, 153, 153, 1),
                        fontSize: 16
                      ),
                      focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Color.fromRGBO(51,51,51,1)),                      
                      ),

                  ),
                )
              ],
            ),
          ),
        ),

         _isLoading ? new CircularProgressIndicator(  backgroundColor: color, valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),) : loginButon
      ],
      crossAxisAlignment: CrossAxisAlignment.center,
    );

    return Scaffold(
        appBar: null,
        key: scaffoldKey,
        backgroundColor: color,
        body: SafeArea(
          child: ListView(
            padding: const EdgeInsets.all(36.0),
            children: <Widget> [
              SizedBox(height: 60.0),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[  
                    new Text(
                      "Sign in to GEMINI",
                      style: TextStyle(fontWeight: FontWeight.bold,color: Color.fromRGBO(38, 62, 114, 1),fontFamily:'Poppins',height: 5, fontSize: 26),
                    ),
                  loginForm,
                  Container(
                    margin: const EdgeInsets.only(top: 20.0),
                    padding: EdgeInsets.all(10),
                    child: Center(
                      child: RichText(
                        text: TextSpan(
                            text: 'Don’t have an account?',
                            style: TextStyle(
                                color: Colors.grey, fontSize: 14,fontWeight: FontWeight.bold),
                            children: <TextSpan>[
                              TextSpan(text: ' Sign up',
                                  style: TextStyle(
                                      color: Color.fromRGBO(51, 51, 51, 1).withOpacity(0.8), fontSize: 16,fontWeight: FontWeight.bold),
                                      recognizer: TapGestureRecognizer()
                                      ..onTap = () async {
                                        _isLoading = await Navigator.push(context,
                                            MaterialPageRoute(builder: (context) => Home()));
                                      },
                              )
                            ]
                        ),
                      ),
                    )
                  ),
                  // FlatButton(                    
                  //   child: Text(
                  //     'Need an Account?',style: TextStyle(color: Colors.black, fontSize: 18)
                  //   ),
                  //   onPressed: () {
                  //     Navigator.push(
                  //       context,
                  //       MaterialPageRoute(
                  //           builder: (context) => CreateAccount(),
                  //           fullscreenDialog: true),
                  //     ).then((success) => success
                  //         ? showAlertPopup(
                  //             context, 'Info', "New Account Created, Login Now.")
                  //         : null);
                  //   },
                  // ),
                ],
              ),
            ],
          ),
        ));
  }

  @override
  void onLoginError(String errorTxt) {
    //_onHideLoading();
    setState(() => _isLoading = false);
   // _showSnackBar(errorTxt);

  }

  @override
  void onLoginSuccess(LoginModel response) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() => _isLoading = false);
    // _onHideLoading();
    //_showSnackBar("Login successful!");
    Navigator.of(_ctx).pushReplacementNamed("/reminder");
    setState(() {
      name = prefs.getString('username');
      isLoggedIn = true;
    });
  }

  void _showDialog(String message) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alert"),
          content: new Text("$message"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _onLoading() {
    showDialog(
      context: scaffoldKey.currentState.context,
      barrierDismissible: false,
      child: new SimpleDialog(
        title: Container(
          child: Center(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 10.0),
                  child: new Text("Loading"),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }


  // void _onHideLoading() {
  //   Navigator.pop(scaffoldKey.currentState.context); //pop dialog
    
  // }

  Future<bool> getLoginState() async{
    SharedPreferences pf =  await SharedPreferences.getInstance();
    bool loginState = pf.getBool('loginState');
    return loginState;
    // return pf.commit();
  }
}



