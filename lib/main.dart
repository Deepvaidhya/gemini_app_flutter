import 'package:flutter/material.dart';
import './routes.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  final color = const Color(0xFFFFFFFF);  

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: color,
          accentColor: Color.fromRGBO(255, 154, 66, 1)
      ),
      routes: routes,
      //home: Scaffold(
       // body: LoginScreen(),
      //),
    );
  }
}
